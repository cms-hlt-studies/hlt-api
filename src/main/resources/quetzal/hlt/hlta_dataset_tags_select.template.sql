@Header
{
	"name": "hlta::dataset::tags_select",

	"fields": {
		"TAG": "string"
	},

	"parameters": {
		"DATASET": "binary"
	}
}

@Body
SELECT
    "TAG"
FROM
    {% table-hlt-dataset-tags %}
WHERE DATASET=:DATASET
ORDER BY "TAG"

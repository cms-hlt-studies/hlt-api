@Header
{
	"name": "hlta::paths::searchByTag",

	"fields": {
		"ID": "binary",
		"NAME": "string"
	},

	"parameters": {
		"TAG": "string"
	}
}

@Body
SELECT DISTINCT
    PATHS.ID,
    PATHS.NAME
FROM
    {% table-hlt-path-tags %} TAGS
    INNER JOIN
    {% table-hlt-paths %} PATHS
    ON PATHS.ID=TAGS.PATH
WHERE UPPER("TAG")=UPPER(:TAG)

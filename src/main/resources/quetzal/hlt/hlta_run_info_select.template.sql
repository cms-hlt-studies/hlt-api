@Header
{
	"name": "hlta::run::info_select",

	"fields": {
		"LHCFILL": "integer",
		"STARTTIME": "string",
		"STOPTIME": "string",
		"HLT_MENU": "string",
		"HLT_DESCRIPTION": "string",
		"HLT_CREATOR": "string",
		"HLT_SW": "string",
		"HLT_ARCH": "string",
		"L1_CONF_KEY": "string",
		"L1_RS_KEY": "string",
		"TRIGGER_MODE": "string"
	},

	"parameters": {
		"RUNNUMBER": "integer"
	}
}

@Body
SELECT
    LHCFILL,
    STARTTIME,
    STOPTIME,
    HLT_MENU,
    HLT_DESCRIPTION,
    HLT_CREATOR,
    HLT_SW,
    HLT_ARCH,
    L1_CONF_KEY,
    L1_RS_KEY,
    TRIGGER_MODE
FROM
    {% table-run-info %}
WHERE RUNNUMBER=:RUNNUMBER

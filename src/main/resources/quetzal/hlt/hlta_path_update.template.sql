@Header
{
	"name": "hlta::path::update",

	"parameters": {
		"PATH": "binary",
		"COMMENTS": "string",
		"TIMESTAMP": "datetime"
	}
}

@Body
UPDATE {% table-hlt-paths %} SET "COMMENTS"=:COMMENTS, "TS_COMMENTS"=:TIMESTAMP
WHERE "ID"=:PATH

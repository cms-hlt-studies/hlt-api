@Header
{
	"name": "hlta::seeds::searchByPeriod",

	"fields": {
		"ID": "binary",
		"NAME": "string",
		"FROM": "datetime",
		"TO": "datetime"
	},

	"parameters": {
		"FROM": "date",
		"TO": "date"
	}
}

@Body
SELECT
    SEEDS.ID,
    MIN(SEEDS.NAME) NAME,
    MIN(RI.STARTTIME) "FROM",
    MAX(RI.STOPTIME) "TO"
FROM
    {% table-l1t-seed-run-info %} SRI
    INNER JOIN
    {% table-l1t-seeds %} SEEDS
    ON SEEDS.ID=SRI.SEED
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=SRI.HLTKEY
WHERE RI.STARTTIME BETWEEN :FROM AND :TO
GROUP BY SEEDS.ID

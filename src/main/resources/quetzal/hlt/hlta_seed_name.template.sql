@Header
{
	"name": "hlta::seed::name",

	"fields": {
		"NAME": "string"
	},

	"parameters": {
		"SEED": "binary"
	}
}

@Body
SELECT "NAME" FROM {% table-l1t-seeds %}
WHERE "ID"=:SEED

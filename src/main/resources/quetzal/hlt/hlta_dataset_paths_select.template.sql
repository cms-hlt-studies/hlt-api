@Header
{
	"name": "hlta::dataset::paths_select",

	"fields": {
		"ID": "binary",
		"NAME": "string",
		"FROM": "integer",
		"TO": "integer",
		"INTLUMI_ACTUAL": "double",
		"INTLUMI_EFFECTIVE": "double"
	},

	"parameters": {
		"DATASET": "binary"
	}
}

@Body
SELECT
    PATHS.ID,
    MIN(PATHS.NAME) "NAME",
    MIN(EXTRACT(YEAR FROM RI.STARTTIME)) "FROM",
    MAX(EXTRACT(YEAR FROM RI.STARTTIME)) "TO",
    SUM(PRI.INTLUMI_ACTUAL) "INTLUMI_ACTUAL",
    SUM(PRI.INTLUMI_EFFECTIVE) "INTLUMI_EFFECTIVE"
FROM
    {% table-hlt-dataset-paths %} DP
    INNER JOIN
    {% table-hlt-paths %} PATHS
    ON PATHS.ID=DP.PATH
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=DP.HLTKEY
    INNER JOIN
    {% table-hlt-path-run-info %} PRI
    ON PRI.RUNNUMBER=RI.RUNNUMBER AND PRI.PATH=DP.PATH
WHERE DP.DATASET=:DATASET
GROUP BY PATHS.ID
ORDER BY "FROM", "NAME", "TO"

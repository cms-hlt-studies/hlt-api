@Header
{
	"name": "hlta::run::dataset_path_prescales_select",

	"fields": {
		"NAME": "string",
		"PRESCALES": "string"
	},

	"parameters": {
		"HLTKEY": "integer",
		"DATASET": "binary"
	}
}

@Body
SELECT
    PATHS.NAME,
    PKI.PRESCALES
FROM
    {% table-hlt-path-key-info %} PKI
    INNER JOIN
    {% table-hlt-paths %} PATHS
    ON PATHS.ID=PKI.PATH
    INNER JOIN
    {% table-hlt-dataset-paths %} DATASET_PATHS
    ON DATASET_PATHS.HLTKEY=PKI.HLTKEY AND DATASET_PATHS.PATH=PKI.PATH
WHERE PKI.HLTKEY=:HLTKEY AND DATASET_PATHS.DATASET=:DATASET

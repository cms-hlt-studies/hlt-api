@Header
{
	"name": "hlta::seeds::searchByTag",

	"fields": {
		"ID": "binary",
		"NAME": "string"
	},

	"parameters": {
		"TAG": "string"
	}
}

@Body
SELECT DISTINCT
    SEEDS.ID,
    SEEDS.NAME
FROM
    {% table-l1t-seed-tags %} TAGS
    INNER JOIN
    {% table-l1t-seeds %} SEEDS
    ON SEEDS.ID=TAGS.SEED
WHERE UPPER("TAG")=UPPER(:TAG)

@Header
{
	"name": "hlta::path::seeds_select",

	"fields": {
		"ID": "binary",
		"NAME": "string",
		"FROM": "integer",
		"TO": "integer",
		"INTLUMI_ACTUAL": "double",
		"INTLUMI_EFFECTIVE": "double"
	},

	"parameters": {
		"PATH": "binary"
	}
}

@Body
SELECT
    SEEDS.ID,
    MIN(SEEDS.NAME) "NAME",
    MIN(EXTRACT(YEAR FROM RI.STARTTIME)) "FROM",
    MAX(EXTRACT(YEAR FROM RI.STARTTIME)) "TO",
    SUM(SRI.INTLUMI_ACTUAL) "INTLUMI_ACTUAL",
    SUM(SRI.INTLUMI_EFFECTIVE) "INTLUMI_EFFECTIVE"
FROM
    {% table-hlt-path-seeds %} PS
    INNER JOIN
    {% table-l1t-seeds %} SEEDS
    ON SEEDS.ID=PS.SEED
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=PS.HLTKEY
    INNER JOIN
    {% table-l1t-seed-run-info %} SRI
    ON SRI.RUNNUMBER=RI.RUNNUMBER AND SRI.SEED=PS.SEED
WHERE PS.PATH=:PATH
GROUP BY SEEDS.ID
ORDER BY "FROM", "NAME", "TO"

@Header
{
	"name": "hlta::dataset::tag_delete",

	"parameters": {
		"DATASET": "binary",
		"TAG": "string"
	}
}

@Body
DELETE FROM {% table-hlt-dataset-tags %}
WHERE "DATASET"=:DATASET AND "TAG"=:TAG

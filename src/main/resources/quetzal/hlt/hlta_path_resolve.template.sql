@Header
{
	"name": "hlta::path::resolve",

	"fields": {
		"ID": "binary"
	},

	"parameters": {
		"NAME": "string"
	}
}

@Body
SELECT "ID" FROM {% table-hlt-paths %}
WHERE "NAME"=:NAME

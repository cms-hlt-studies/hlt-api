@Header
{
	"name": "hlta::run::seed_prescales_select",

	"fields": {
		"PRESCALES": "string"
	},

	"parameters": {
		"RUNNUMBER": "integer",
		"SEED": "binary"
	}
}

@Body
SELECT
    PRESCALES
FROM
    {% table-l1t-seed-run-info %}
WHERE RUNNUMBER=:RUNNUMBER AND SEED=:SEED

@Header
{
	"name": "hlta::path::key_info_select",

	"fields": {
		"ID_PATHID": "integer",
		"DESCRIPTION": "string",
		"CONTACT": "string",
		"VERSION": "string",
		"PRESCALES": "string",
		"SEED_EXPRESSION": "string"
	},

	"parameters": {
		"HLTKEY": "integer",
		"PATH": "binary"
	}
}

@Body
SELECT
    ID_PATHID,
    DESCRIPTION,
    CONTACT,
    VERSION,
    PRESCALES,
    SEED_EXPRESSION
FROM
    {% table-hlt-path-key-info %}
WHERE HLTKEY=:HLTKEY AND "PATH"=:PATH

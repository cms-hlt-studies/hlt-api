@Header
{
	"name": "hlta::path::name",

	"fields": {
		"NAME": "string"
	},

	"parameters": {
		"PATH": "binary"
	}
}

@Body
SELECT "NAME" FROM {% table-hlt-paths %}
WHERE "ID"=:PATH

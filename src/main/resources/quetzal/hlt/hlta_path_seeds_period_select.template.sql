@Header
{
	"name": "hlta::path::seeds_period_select",

	"fields": {
		"ID": "binary",
		"NAME": "string"
	},

	"parameters": {
		"PATH": "binary",
		"FROM": "datetime",
		"TO": "datetime"
	}
}

@Body
SELECT DISTINCT SEEDS.ID, SEEDS.NAME FROM
    {% table-hlt-path-seeds %} PATH_SEEDS
    INNER JOIN
    {% table-l1t-seeds %} SEEDS
    ON SEEDS.ID=PATH_SEEDS.SEED
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=PATH_SEEDS.HLTKEY
WHERE PATH_SEEDS.PATH=:PATH AND RI."STARTTIME" BETWEEN :FROM AND :TO
ORDER BY SEEDS.NAME

@Header
{
	"name": "hlta::path::tags_select",

	"fields": {
		"TAG": "string"
	},

	"parameters": {
		"PATH": "binary"
	}
}

@Body
SELECT
    "TAG"
FROM
    {% table-hlt-path-tags %}
WHERE "PATH"=:PATH
ORDER BY "TAG"

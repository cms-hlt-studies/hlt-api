@Header
{
	"name": "hlta::run::prescale_indexes_period_select",

	"fields": {
		"INDEX": "integer",
		"RUNNUMBER": "integer",
		"FROM": "integer",
		"TO": "integer",
		"INTLUMI": "double"
	},

	"parameters": {
		"FROM": "datetime",
		"TO": "datetime"
	}
}

@Body
SELECT INDEXES."INDEX", INFO."RUNNUMBER", INDEXES."FROM", INDEXES."TO", INDEXES."INTLUMI" FROM
    {% table-run-prescale-indexes %} INDEXES
    INNER JOIN
    {% table-run-info %} INFO
    ON INFO.RUNNUMBER=INDEXES.RUNNUMBER
WHERE INFO."STARTTIME" BETWEEN :FROM AND :TO
ORDER BY INFO."RUNNUMBER", INDEXES."FROM"

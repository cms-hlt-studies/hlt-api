@Header
{
	"name": "hlta::dataset::paths_period_select",

	"fields": {
		"ID": "binary",
		"NAME": "string"
	},

	"parameters": {
		"DATASET": "binary",
		"FROM": "datetime",
		"TO": "datetime"
	}
}

@Body
SELECT DISTINCT PATHS.ID, PATHS.NAME FROM
    {% table-hlt-dataset-paths %} DATASET_PATHS
    INNER JOIN
    {% table-hlt-paths %} PATHS
    ON PATHS.ID=DATASET_PATHS.PATH
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=DATASET_PATHS.HLTKEY
WHERE DATASET_PATHS.DATASET=:DATASET AND RI."STARTTIME" BETWEEN :FROM AND :TO
ORDER BY PATHS.NAME

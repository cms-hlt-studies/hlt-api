@Header
{
	"name": "hlta::datasets::searchByTag",

	"fields": {
		"ID": "binary",
		"NAME": "string"
	},

	"parameters": {
		"TAG": "string"
	}
}

@Body
SELECT DISTINCT
    DATASETS.ID,
    DATASETS.NAME
FROM
    {% table-hlt-dataset-tags %} TAGS
    INNER JOIN
    {% table-hlt-datasets %} DATASETS
    ON DATASETS.ID=TAGS.DATASET
WHERE UPPER("TAG")=UPPER(:TAG)

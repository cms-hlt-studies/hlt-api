@Header
{
	"name": "hlta::paths::searchBySeed",

	"fields": {
		"ID": "binary",
		"NAME": "string"
	},

	"parameters": {
		"SEED": "string"
	}
}

@Body
SELECT DISTINCT
    PATHS.ID,
    PATHS.NAME
FROM
    {% table-hlt-path-seeds %} PATH_SEEDS
    INNER JOIN
    {% table-l1t-seeds %} SEEDS
    ON SEEDS.ID=PATH_SEEDS.SEED
    INNER JOIN
    {% table-hlt-paths %} PATHS
    ON PATHS.ID=PATH_SEEDS.PATH
WHERE UPPER(SEEDS.NAME) LIKE UPPER(:SEED)

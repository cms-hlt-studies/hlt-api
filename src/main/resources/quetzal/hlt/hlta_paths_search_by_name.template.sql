@Header
{
	"name": "hlta::paths::searchByName",

	"fields": {
		"ID": "binary",
		"NAME": "string"
	},

	"parameters": {
		"QUERY": "string"
	}
}

@Body
SELECT DISTINCT
    "ID",
    "NAME"
FROM
    {% table-hlt-paths %}
WHERE UPPER("NAME") LIKE UPPER(:QUERY)

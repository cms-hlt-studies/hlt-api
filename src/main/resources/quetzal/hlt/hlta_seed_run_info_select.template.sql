@Header
{
	"name": "hlta::seed::run_info_select",

	"fields": {
		"PRESCALES": "string"
	},

	"parameters": {
		"RUNNUMBER": "integer",
		"SEED": "binary"
	}
}

@Body
SELECT
    PRESCALES
FROM
    {% table-l1t-seed-run-info %}
WHERE RUNNUMBER=:RUNNUMBER AND "SEED"=:SEED

@Header
{
	"name": "hlta::seed::tag_create",

	"parameters": {
		"SEED": "binary",
		"TAG": "string"
	}
}

@Body
INSERT INTO {% table-l1t-seed-tags %} ("SEED", "TAG")
VALUES (:SEED, :TAG)

@Header
{
	"name": "hlta::seeds::searchByName",

	"fields": {
		"ID": "binary",
		"NAME": "string"
	},

	"parameters": {
		"QUERY": "string"
	}
}

@Body
SELECT DISTINCT
    "ID",
    "NAME"
FROM
    {% table-l1t-seeds %}
WHERE UPPER("NAME") LIKE UPPER(:QUERY)

@Header
{
	"name": "hlta::dataset::tag_create",

	"parameters": {
		"DATASET": "binary",
		"TAG": "string"
	}
}

@Body
INSERT INTO {% table-hlt-dataset-tags %} ("DATASET", "TAG")
VALUES (:DATASET, :TAG)

@Header
{
	"name": "hlta::seed::tag_delete",

	"parameters": {
		"SEED": "binary",
		"TAG": "string"
	}
}

@Body
DELETE FROM {% table-l1t-seed-tags %}
WHERE "SEED"=:SEED AND "TAG"=:TAG

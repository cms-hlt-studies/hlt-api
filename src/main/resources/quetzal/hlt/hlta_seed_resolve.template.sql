@Header
{
	"name": "hlta::seed::resolve",

	"fields": {
		"ID": "binary"
	},

	"parameters": {
		"NAME": "string"
	}
}

@Body
SELECT "ID" FROM {% table-l1t-seeds %}
WHERE "NAME"=:NAME

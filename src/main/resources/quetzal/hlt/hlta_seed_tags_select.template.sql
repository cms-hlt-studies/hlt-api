@Header
{
	"name": "hlta::seed::tags_select",

	"fields": {
		"TAG": "string"
	},

	"parameters": {
		"SEED": "binary"
	}
}

@Body
SELECT
    "TAG"
FROM
    {% table-l1t-seed-tags %}
WHERE SEED=:SEED
ORDER BY "TAG"

@Header
{
	"name": "hlta::dataset::summaries_select",

	"fields": {
		"YEAR": "integer",
		"INTLUMI_ACTUAL": "double",
		"INTLUMI_EFFECTIVE": "double"
	},

	"parameters": {
		"DATASET": "binary"
	}
}

@Body
SELECT DISTINCT
    "KEY" "YEAR",
    INTLUMI_MAX_ACTUAL INTLUMI_ACTUAL,
    INTLUMI_MAX_EFFECTIVE INTLUMI_EFFECTIVE
FROM
    {% table-hlt-dataset-summaries %}
WHERE "DATASET"=:DATASET AND KIND='YEARLY'
ORDER BY "YEAR"

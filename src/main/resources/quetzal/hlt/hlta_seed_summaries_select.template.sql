@Header
{
	"name": "hlta::seed::summaries_select",

	"fields": {
		"YEAR": "integer",
		"INTLUMI_ACTUAL": "double",
		"INTLUMI_EFFECTIVE": "double"
	},

	"parameters": {
		"SEED": "binary"
	}
}

@Body
SELECT DISTINCT
    "KEY" "YEAR",
    INTLUMI_ACTUAL,
    INTLUMI_EFFECTIVE
FROM
    {% table-l1t-seed-summaries %}
WHERE "SEED"=:SEED AND KIND='YEARLY'
ORDER BY "YEAR"

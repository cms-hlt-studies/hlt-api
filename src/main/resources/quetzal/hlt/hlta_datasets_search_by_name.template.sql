@Header
{
	"name": "hlta::datasets::searchByName",

	"fields": {
		"ID": "binary",
		"NAME": "string"
	},

	"parameters": {
		"QUERY": "string"
	}
}

@Body
SELECT DISTINCT
    "ID",
    "NAME"
FROM
    {% table-hlt-datasets %}
WHERE UPPER("NAME") LIKE UPPER(:QUERY)

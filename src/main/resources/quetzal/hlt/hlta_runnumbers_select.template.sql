@Header
{
	"name": "hlta::runnumbers_select",

	"fields": {
		"RUNNUMBER": "integer",
		"HLTKEY": "integer",
		"HLTMENU": "string",
		"STARTTIME": "datetime",
		"STOPTIME": "datetime"
	},

	"parameters": {
		"FROM": "datetime",
		"TO": "datetime"
	}
}

@Body
SELECT
    RUNNUMBER,
    HLT_KEY HLTKEY,
    HLT_MENU HLTMENU,
    STARTTIME,
    STOPTIME
FROM
    {% table-run-info %}
WHERE STARTTIME BETWEEN :FROM AND :TO
ORDER BY RUNNUMBER

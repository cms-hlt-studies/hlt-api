@Header
{
	"name": "hlta::paths::searchByPeriod",

	"fields": {
		"ID": "binary",
		"NAME": "string",
		"FROM": "datetime",
		"TO": "datetime"
	},

	"parameters": {
		"FROM": "date",
		"TO": "date"
	}
}

@Body
SELECT
    PATHS.ID,
    MIN(PATHS.NAME) NAME,
    MIN(RI.STARTTIME) "FROM",
    MAX(RI.STOPTIME) "TO"
FROM
    {% table-hlt-path-key-info %} PKI
    INNER JOIN
    {% table-hlt-paths %} PATHS
    ON PATHS.ID=PKI.PATH
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=PKI.HLTKEY
WHERE RI.STARTTIME BETWEEN :FROM AND :TO
GROUP BY PATHS.ID

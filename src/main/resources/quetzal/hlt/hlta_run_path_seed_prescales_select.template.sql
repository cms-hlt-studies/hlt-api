@Header
{
	"name": "hlta::run::path_seed_prescales_select",

	"fields": {
		"NAME": "string",
		"PRESCALES": "string"
	},

	"parameters": {
		"RUNNUMBER": "integer",
		"HLTKEY": "integer",
		"PATH": "binary"
	}
}

@Body
SELECT
    SEEDS.NAME,
    SRI.PRESCALES
FROM
    {% table-l1t-seed-run-info %} SRI
    INNER JOIN
    {% table-l1t-seeds %} SEEDS
    ON SEEDS.ID=SRI.SEED
    INNER JOIN
    {% table-hlt-path-seeds %} PATH_SEEDS
    ON PATH_SEEDS.SEED=SRI.SEED
WHERE SRI.RUNNUMBER=:RUNNUMBER AND PATH_SEEDS.HLTKEY=:HLTKEY AND PATH_SEEDS.PATH=:PATH

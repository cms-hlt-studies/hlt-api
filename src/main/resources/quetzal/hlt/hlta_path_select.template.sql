@Header
{
	"name": "hlta::path::select",

	"fields": {
		"NAME": "string",
		"COMMENTS": "string"
	},

	"parameters": {
		"PATH": "binary"
	}
}

@Body
SELECT
    "NAME",
    "COMMENTS"
FROM
    {% table-hlt-paths %}
WHERE "ID"=:PATH

@Header
{
	"name": "hlta::dataset::update",

	"parameters": {
		"DATASET": "binary",
		"COMMENTS": "string",
		"TIMESTAMP": "datetime"
	}
}

@Body
UPDATE {% table-hlt-datasets %} SET "COMMENTS"=:COMMENTS, "TS_COMMENTS"=:TIMESTAMP
WHERE "ID"=:DATASET

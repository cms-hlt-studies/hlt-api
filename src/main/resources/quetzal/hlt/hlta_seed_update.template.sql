@Header
{
	"name": "hlta::seed::update",

	"parameters": {
		"SEED": "binary",
		"COMMENTS": "string",
		"TIMESTAMP": "datetime"
	}
}

@Body
UPDATE {% table-l1t-seeds %} SET "COMMENTS"=:COMMENTS, "TS_COMMENTS"=:TIMESTAMP
WHERE "ID"=:SEED

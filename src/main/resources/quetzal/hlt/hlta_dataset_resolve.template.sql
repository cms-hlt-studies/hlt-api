@Header
{
	"name": "hlta::dataset::resolve",

	"fields": {
		"ID": "binary"
	},

	"parameters": {
		"NAME": "string"
	}
}

@Body
SELECT "ID" FROM {% table-hlt-datasets %}
WHERE "NAME"=:NAME

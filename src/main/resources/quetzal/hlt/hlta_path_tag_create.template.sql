@Header
{
	"name": "hlta::path::tag_create",

	"parameters": {
		"PATH": "binary",
		"TAG": "string"
	}
}

@Body
INSERT INTO {% table-hlt-path-tags %} ("PATH", "TAG")
VALUES (:PATH, :TAG)

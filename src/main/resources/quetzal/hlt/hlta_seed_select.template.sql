@Header
{
	"name": "hlta::seed::select",

	"fields": {
		"NAME": "string",
		"COMMENTS": "string"
	},

	"parameters": {
		"SEED": "binary"
	}
}

@Body
SELECT
    "NAME",
    "COMMENTS"
FROM
    {% table-l1t-seeds %}
WHERE "ID"=:SEED

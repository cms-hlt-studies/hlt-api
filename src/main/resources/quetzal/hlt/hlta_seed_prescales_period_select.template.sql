@Header
{
	"name": "hlta::seed::prescales_period_select",

	"fields": {
		"RUNNUMBER": "integer",
		"PRESCALES": "sql_clob"
	},

	"parameters": {
		"SEED": "binary",
		"FROM": "datetime",
		"TO": "datetime"
	}
}

@Body
SELECT
    RI.RUNNUMBER,
    SRI.PRESCALES
FROM
    {% table-l1t-seed-run-info %} SRI
    INNER JOIN
    {% table-run-info %} RI
    ON RI.RUNNUMBER=SRI.RUNNUMBER
WHERE SRI.SEED=:SEED AND RI."STARTTIME" BETWEEN :FROM AND :TO

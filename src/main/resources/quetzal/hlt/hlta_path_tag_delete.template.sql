@Header
{
	"name": "hlta::path::tag_delete",

	"parameters": {
		"PATH": "binary",
		"TAG": "string"
	}
}

@Body
DELETE FROM {% table-hlt-path-tags %}
WHERE "PATH"=:PATH AND "TAG"=:TAG

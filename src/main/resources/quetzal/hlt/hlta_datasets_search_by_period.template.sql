@Header
{
	"name": "hlta::datasets::searchByPeriod",

	"fields": {
		"ID": "binary",
		"NAME": "string",
		"FROM": "datetime",
		"TO": "datetime"
	},

	"parameters": {
		"FROM": "date",
		"TO": "date"
	}
}

@Body
SELECT
    DATASETS.ID,
    MIN(DATASETS.NAME) NAME,
    MIN(RI.STARTTIME) "FROM",
    MAX(RI.STOPTIME) "TO"
FROM
    {% table-hlt-dataset-key-info %} DKI
    INNER JOIN
    {% table-hlt-datasets %} DATASETS
    ON DATASETS.ID=DKI.DATASET
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=DKI.HLTKEY
WHERE RI.STARTTIME BETWEEN :FROM AND :TO
GROUP BY DATASETS.ID

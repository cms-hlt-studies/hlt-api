@Header
{
	"name": "hlta::dataset::select",

	"fields": {
		"NAME": "string",
		"COMMENTS": "string"
	},

	"parameters": {
		"DATASET": "binary"
	}
}

@Body
SELECT
    "NAME",
    "COMMENTS"
FROM
    {% table-hlt-datasets %}
WHERE "ID"=:DATASET

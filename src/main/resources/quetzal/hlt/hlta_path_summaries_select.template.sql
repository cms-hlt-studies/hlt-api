@Header
{
	"name": "hlta::path::summaries_select",

	"fields": {
		"YEAR": "integer",
		"INTLUMI_ACTUAL": "double",
		"INTLUMI_EFFECTIVE": "double"
	},

	"parameters": {
		"PATH": "binary"
	}
}

@Body
SELECT DISTINCT
    "KEY" "YEAR",
    INTLUMI_ACTUAL,
    INTLUMI_EFFECTIVE
FROM
    {% table-hlt-path-summaries %}
WHERE "PATH"=:PATH AND KIND='YEARLY'
ORDER BY "YEAR"

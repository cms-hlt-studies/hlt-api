@Header
{
	"name": "hlta::run::path_key_info_select",

	"fields": {
		"ID": "binary",
		"ID_PATHID": "integer",
		"NAME": "string",
		"DESCRIPTION": "string",
		"CONTACT": "string",
		"VERSION": "string",
		"PRESCALES": "string",
		"SEED_EXPRESSION": "string"
	},

	"parameters": {
		"RUNNUMBER": "integer"
	}
}

@Body
SELECT
    PATHS.ID,
    PKI.ID_PATHID,
    PATHS.NAME,
    PKI.DESCRIPTION,
    PKI.CONTACT,
    PKI.VERSION,
    PKI.PRESCALES,
    PKI.SEED_EXPRESSION
FROM
    {% table-hlt-path-key-info %} PKI
    INNER JOIN
    {% table-hlt-paths %} PATHS
    ON PATHS.ID=PKI.PATH
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=PKI.HLTKEY
WHERE RI.RUNNUMBER=:RUNNUMBER

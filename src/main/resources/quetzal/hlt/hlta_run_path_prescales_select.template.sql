@Header
{
	"name": "hlta::run::path_prescales_select",

	"fields": {
		"PRESCALES": "string"
	},

	"parameters": {
		"RUNNUMBER": "integer",
		"PATH": "binary"
	}
}

@Body
SELECT
    KI.PRESCALES
FROM
    {% table-hlt-path-key-info %} KI
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=HPP.HLTKEY
WHERE RI.RUNNUMBER=:RUNNUMBER AND KI.PATH=:PATH

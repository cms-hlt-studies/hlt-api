@Header
{
	"name": "hlta::path::prescales_period_select",

	"fields": {
		"RUNNUMBER": "integer",
		"PRESCALES": "sql_clob"
	},

	"parameters": {
		"PATH": "binary",
		"FROM": "datetime",
		"TO": "datetime"
	}
}

@Body
SELECT
    RI.RUNNUMBER,
    PKI.PRESCALES
FROM
    {% table-hlt-path-key-info %} PKI
    INNER JOIN
    {% table-run-info %} RI
    ON RI.HLT_KEY=PKI.HLTKEY
WHERE PKI.PATH=:PATH AND RI."STARTTIME" BETWEEN :FROM AND :TO

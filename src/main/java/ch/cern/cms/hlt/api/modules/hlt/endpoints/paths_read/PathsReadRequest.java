/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.paths_read;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;

import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class PathsReadRequest {

    private final byte[] path;

    public PathsReadRequest(CERNClientContext ctx) {
        Map<String, String> path = ctx.pathParamMap();

        this.path = dp()
            .require("path")
            .asBinary()
            .notNull()
            .apply(path);
    }

    public byte[] getPath() {
        return path;
    }

}

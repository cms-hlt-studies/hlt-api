package ch.cern.cms.hlt.api.modules.security.endpoints.user_login;

import ch.cern.cms.hlt.api.HLTApiRuntime;
import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.SSOAuthentication;
import ch.cern.cms.hlt.api.modules.security.authentication.UserToken;
import ch.cern.cms.hlt.api.modules.security.resources.UserRecord;
import ch.cern.cms.hlt.api.modules.security.resources.UsersFile;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientProfile;
import com.smmx.analysis.quetzal.core.http.errors.ServerException;
import com.smmx.analysis.quetzal.core.http.handlers.ClientScopes;

import java.net.URI;
import java.net.URISyntaxException;

import static com.smmx.analysis.quetzal.core.utils.CoreUtils.scopes;

public class UserLoginHandler extends CERNClientHandler {

    public UserLoginHandler() {
        // SECURITY
        this.registerAuthenticationMethod(new SSOAuthentication());

        if (HLTApiRuntime.debug_en) {
            this.permitScope(ClientScopes.ROLE_ANYONE);
        } else {
            this.permitScope(SecurityModule.ROLE_USER_AUTHENTICATED);
        }
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // REQUEST
        UserLoginRequest request = new UserLoginRequest(ctx);

        // PROFILE
        CERNClientProfile profile = ctx.getProfile();

        // BUILD TOKEN
        UserToken token = new UserToken();
        token.setTokenId(profile.getSessionId());
        token.setNICEAccountId(profile.getNICEAccountId());
        token.setScopes(scopes(
            SecurityModule.ROLE_USER_TOKEN
        ));

        UserRecord user_record = UsersFile.getInstance().getUser(
            profile.getNICEAccountId()
        );

        if (HLTApiRuntime.setup_en) {
            token.getScopes().add(SecurityModule.ROLE_ADMIN);
        } else if (user_record != null) {
            token.getScopes().addAll(user_record.getRoles());
        }

        if (HLTApiRuntime.debug_en) {
            token.getScopes().add(SecurityModule.ROLE_MAINTAINER);
        }

        // BUILD URL
        URI uri;

        try {
            uri = new URI(request.getCallback());

            String new_query = uri.getQuery();
            String signed_token = "token=" + token.sign();

            if (new_query == null) {
                new_query = signed_token;
            } else {
                new_query += "&" + signed_token;
            }

            uri = new URI(
                uri.getScheme(),
                uri.getAuthority(),
                uri.getPath(),
                new_query,
                uri.getFragment()
            );
        } catch (URISyntaxException ex) {
            throw new ServerException("Failed to build callback.", ex);
        }

        // REDIRECT
        ctx.redirect(uri.toString());
    }

}

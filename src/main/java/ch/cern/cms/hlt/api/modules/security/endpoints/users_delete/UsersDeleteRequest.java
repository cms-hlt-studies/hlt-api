/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.endpoints.users_delete;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;

import java.util.Map;
import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class UsersDeleteRequest {

    private final String user;

    public UsersDeleteRequest(CERNClientContext ctx) {
        Map<String, String> path = ctx.pathParamMap();

        this.user = dp()
            .require("user")
            .asString()
            .notNull()
            .check(
                input -> {
                    return !Objects.equals(
                        ctx.getProfile().getClientId(),
                        input
                    );
                },
                "Operation is not allowed."
            )
            .apply(path);
    }

    public String getUser() {
        return user;
    }

}

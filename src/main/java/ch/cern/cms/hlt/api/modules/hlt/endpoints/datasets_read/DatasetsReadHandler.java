/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.datasets_read;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.AppAuthentication;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import com.smmx.analysis.quetzal.core.http.errors.ServerException;
import com.smmx.analysis.quetzal.core.http.errors.responses.ServerErrorResponse;
import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionShield;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.smmx.analysis.quetzal.QuetzalUtils.readConnection;
import static com.smmx.analysis.quetzal.QuetzalUtils.sqlt;
import static com.smmx.maria.commons.MariaCommons.dict;
import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class DatasetsReadHandler extends CERNClientHandler {

    public DatasetsReadHandler() {
        this.registerAuthenticationMethod(new AppAuthentication());
        this.permitScope(SecurityModule.PRIVILEGE_MINE);

        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_USER_TOKEN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // REQUEST
        DatasetsReadRequest request = new DatasetsReadRequest(ctx);

        // GET DATA
        Map<String, Object> data;

        try (Loan<ConnectionShield> loan = readConnection()) {
            data = sqlt("hlta::dataset::select")
                .preparedStatement(loan.getObject())
                .queryAndGetFirstMap(dict(
                    "DATASET", request.getDataset()
                ))
                .orElse(null);
        } catch (SQLException ex) {
            throw new ServerException("Failed to get data.", ex);
        }

        // GET TAGS
        List<String> tags;

        try (Loan<ConnectionShield> loan = readConnection()) {
            tags = sqlt("hlta::dataset::tags_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "DATASET", request.getDataset()
                ))
                .stream()
                .map(dp().get("TAG").asString())
                .collect(Collectors.toList());
        } catch (SQLException ex) {
            throw new ServerException("Failed to get tags.", ex);
        }

        // GET SUMMARIES
        List<Map<String, Object>> summaries;

        try (Loan<ConnectionShield> loan = readConnection()) {
            summaries = sqlt("hlta::dataset::summaries_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "DATASET", request.getDataset()
                ));
        } catch (SQLException ex) {
            throw new ServerErrorResponse("Failed to get summaries.", ex);
        }

        // GET PATHS
        List<Map<String, Object>> paths;

        try (Loan<ConnectionShield> loan = readConnection()) {
            paths = sqlt("hlta::dataset::paths_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "DATASET", request.getDataset()
                ));
        } catch (SQLException ex) {
            throw new ServerErrorResponse("Failed to get datasets.", ex);
        }

        // REPLY
        ctx.result(dict(
            data,
            "ID", request.getDataset(),
            "TAGS", tags,
            "SUMMARIES", summaries,
            "PATHS", paths
        ));
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.paths;

import ch.cern.cms.hlt.api.http.handlers.query.OracleSQLQueryUtils;
import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.AppAuthentication;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import com.smmx.analysis.quetzal.core.http.errors.responses.ClientErrorResponse;
import com.smmx.maria.commons.database.sql.SQL;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.smmx.analysis.quetzal.QuetzalUtils.sqlt;
import static com.smmx.maria.commons.MariaCommons.*;

/**
 * @author omiguelc
 */
public class PathsHandler extends CERNClientHandler {

    public PathsHandler() {
        this.registerAuthenticationMethod(new AppAuthentication());
        this.permitScope(SecurityModule.PRIVILEGE_MINE);

        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_USER_TOKEN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        PathsRequest request = new PathsRequest(ctx);

        // GET SELECT QUERY
        SQL select_snippet;
        Map<String, Object> select_args = new HashMap<>();

        if (request.getQuery() != null) {
            select_snippet = sqlt("hlta::paths::searchByName");
            select_args.put("QUERY", request.getQuery());
        } else if (request.getSeed() != null) {
            select_snippet = sqlt("hlta::paths::searchBySeed");
            select_args.put("SEED", request.getSeed());
        } else if (request.getTag() != null) {
            select_snippet = sqlt("hlta::paths::searchByTag");
            select_args.put("TAG", request.getTag());
        } else {
            throw new ClientErrorResponse("No query parameters given.");
        }

        // QUERY
        List<Map<String, Object>> entries = OracleSQLQueryUtils.query(
            ctx,
            sql(mlstr(
                "SELECT * FROM (",
                "   SELECT",
                "       RES.ID,",
                "       MAX(RES.NAME) NAME,",
                "       LISTAGG(S.KEY, ', ') \"YEARS\",",
                "       MAX(CASE WHEN S.KEY != '" + request.getYear() + "' THEN NULL ELSE S.INTLUMI_ACTUAL END) INTLUMI_ACTUAL,",
                "       MAX(CASE WHEN S.KEY != '" + request.getYear() + "' THEN NULL ELSE S.INTLUMI_EFFECTIVE END) INTLUMI_EFFECTIVE",
                "   FROM",
                "       ({% __select__ %}) RES",
                "       INNER JOIN",
                "       {% table-hlt-path-summaries %} S",
                "       ON S.PATH=RES.ID AND S.KIND='YEARLY'",
                "   GROUP BY RES.ID",
                ") TEMP",
                "WHERE \"YEARS\" LIKE '%" + request.getYear() + "%'",
                "{% __order_by__ %}",
                "{% __pagination__ %}"
            ))
                .registerField("YEARS", String.class)
                .registerField("INTLUMI_ACTUAL", Double.class)
                .registerField("INTLUMI_EFFECTIVE", Double.class),
            select_snippet,
            select_args,
            sql("ORDER BY \"NAME\" ASC")
        );

        // REPLY
        ctx.result(dict(
            "entries", entries
        ));
    }

}

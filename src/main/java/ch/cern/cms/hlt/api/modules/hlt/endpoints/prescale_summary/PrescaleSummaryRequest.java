/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.prescale_summary;

import ch.cern.cms.hlt.api.modules.hlt.enums.HLTObjectType;
import ch.cern.cms.hlt.api.modules.hlt.resources.Datasets;
import ch.cern.cms.hlt.api.modules.hlt.resources.Paths;
import ch.cern.cms.hlt.api.modules.hlt.utils.DatasetUtils;
import ch.cern.cms.hlt.api.modules.hlt.utils.PathUtils;
import ch.cern.cms.hlt.api.modules.hlt.utils.SeedUtils;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import com.smmx.analysis.quetzal.core.http.errors.responses.ClientErrorResponse;

import java.util.*;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.dp;
import static com.smmx.maria.commons.MariaCommons.list;

/**
 * @author omiguelc
 */
public class PrescaleSummaryRequest {

    private final Date from;
    private final Date to;
    private final List<HLTObject> objects;

    public PrescaleSummaryRequest(CERNClientContext ctx) {
        Map<String, Object> req = ctx.bodyParamMap();

        this.from = dp()
            .require("from")
            .asDate()
            .notNull()
            .apply(req);

        this.to = dp()
            .require("to")
            .asDate()
            .notNull()
            .apply(req);

        // OBJECT OPTIONS
        List<HLTObject> given_objects = dp()
            .get("objects", Collections.emptyList())
            .asList((item) -> new HLTObject((Map<String, Object>) item))
            .map(value -> {
                if (value.isEmpty()) {
                    return null;
                }

                return value;
            })
            .apply(req);

        String dataset_name = dp()
            .get("dataset_name")
            .asString()
            .apply(req);

        byte[] dataset = dp()
            .get("dataset")
            .asBinary()
            .apply(req);

        String path_name = dp()
            .get("path_name")
            .asString()
            .apply(req);

        byte[] path = dp()
            .get("path")
            .asBinary()
            .apply(req);

        // SANITY CHECKS
        List<Object> object_options = list(
            given_objects,
            dataset_name, dataset,
            path_name, path
        );

        if (object_options.stream().allMatch(Objects::isNull)) {
            throw new ClientErrorResponse("Objects weren't given.");
        } else if (object_options.stream().filter(Objects::nonNull).count() > 1) {
            throw new ClientErrorResponse("Objects can only be specified using one method.");
        }

        // RESOLVE
        if (given_objects != null) {
            this.objects = given_objects;
        } else if (dataset != null || dataset_name != null) {
            byte[] dataset_id = DatasetUtils.getDatasetID(dataset, dataset_name);

            List<Map<String, Object>> period_paths = Datasets.getPathsForPeriod(
                dataset_id,
                from,
                to
            );

            this.objects = period_paths.stream()
                .map(path_data -> {
                    byte[] _path_id = (byte[]) path_data.get("ID");
                    String _path_name = (String) path_data.get("NAME");

                    return new HLTObject(HLTObjectType.PATH, _path_id, _path_name);
                })
                .collect(Collectors.toList());
        } else if (path != null || path_name != null) {
            byte[] path_id = PathUtils.getPathID(path, path_name);

            List<Map<String, Object>> period_seeds = Paths.getSeedsForPeriod(
                path_id,
                from,
                to
            );

            this.objects = new ArrayList<>();

            this.objects.add(new HLTObject(HLTObjectType.PATH, path_id, path_name));

            this.objects.addAll(
                period_seeds.stream()
                    .map(seed_data -> {
                        byte[] _seed_id = (byte[]) seed_data.get("ID");
                        String _seed_name = (String) seed_data.get("NAME");

                        return new HLTObject(HLTObjectType.SEED, _seed_id, _seed_name);
                    })
                    .collect(Collectors.toList())
            );
        } else {
            throw new ClientErrorResponse("Where has this world gone...");
        }

    }

    public Date getFrom() {
        return from;
    }

    public Date getTo() {
        return to;
    }

    public List<HLTObject> getObjects() {
        return objects;
    }

    public class HLTObject {

        private final HLTObjectType type;
        private final byte[] id;
        private final String name;

        public HLTObject(HLTObjectType type, byte[] id, String name) {
            this.type = type;
            this.id = id;
            this.name = name;
        }

        public HLTObject(Map<String, Object> root) {
            this.type = dp()
                .require("type")
                .asEnum(HLTObjectType.class)
                .ifNull(HLTObjectType.PATH)
                .apply(root);

            byte[] id = dp()
                .require("id")
                .asBinary()
                .apply(root);

            String name = dp()
                .require("name")
                .asString()
                .apply(root);

            if (HLTObjectType.PATH.equals(type)) {
                this.id = PathUtils.getPathID(id, name);
            } else {
                this.id = SeedUtils.getSeedID(id, name);
            }

            this.name = name;
        }

        public HLTObjectType getType() {
            return type;
        }

        public byte[] getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

}

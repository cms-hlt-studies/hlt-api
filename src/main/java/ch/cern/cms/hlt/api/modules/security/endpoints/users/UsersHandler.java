package ch.cern.cms.hlt.api.modules.security.endpoints.users;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.resources.UsersFile;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Scope;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.dict;

public class UsersHandler extends CERNClientHandler {

    public UsersHandler() {
        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_ADMIN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // GET SERVICES
        List<Map<String, Object>> apps = UsersFile.getInstance().getAllUsers()
            .stream()
            .map(record -> dict(
                "ID", record.getNICEAccountId(),
                "ROLES", record.getRoles().stream()
                    .map(Scope::getName)
                    .collect(Collectors.toSet()),
                "TIMESTAMP", record.getTimestamp()
            ))
            .collect(Collectors.toList());

        // REPLY
        ctx.result(dict(
            "ENTRIES", apps
        ));
    }

}

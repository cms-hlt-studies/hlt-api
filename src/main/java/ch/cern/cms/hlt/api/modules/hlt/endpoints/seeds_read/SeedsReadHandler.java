/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.seeds_read;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.AppAuthentication;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import com.smmx.analysis.quetzal.core.http.errors.ServerException;
import com.smmx.analysis.quetzal.core.http.errors.responses.ServerErrorResponse;
import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionShield;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.smmx.analysis.quetzal.QuetzalUtils.readConnection;
import static com.smmx.analysis.quetzal.QuetzalUtils.sqlt;
import static com.smmx.maria.commons.MariaCommons.dict;
import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class SeedsReadHandler extends CERNClientHandler {

    public SeedsReadHandler() {
        this.registerAuthenticationMethod(new AppAuthentication());
        this.permitScope(SecurityModule.PRIVILEGE_MINE);

        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_USER_TOKEN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // REQUEST
        SeedsReadRequest request = new SeedsReadRequest(ctx);

        // GET DATA
        Map<String, Object> data;

        try (Loan<ConnectionShield> loan = readConnection()) {
            data = sqlt("hlta::seed::select")
                .preparedStatement(loan.getObject())
                .queryAndGetFirstMap(dict(
                    "SEED", request.getSeed()
                ))
                .orElse(null);
        } catch (SQLException ex) {
            throw new ServerException("Failed to get data.", ex);
        }

        // GET TAGS
        List<String> tags;

        try (Loan<ConnectionShield> loan = readConnection()) {
            tags = sqlt("hlta::seed::tags_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "SEED", request.getSeed()
                ))
                .stream()
                .map(dp().get("TAG").asString())
                .collect(Collectors.toList());
        } catch (SQLException ex) {
            throw new ServerException("Failed to get tags.", ex);
        }

        // GET YEARS
        List<Map<String, Object>> summaries;

        try (Loan<ConnectionShield> loan = readConnection()) {
            summaries = sqlt("hlta::seed::summaries_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "SEED", request.getSeed()
                ));
        } catch (SQLException ex) {
            throw new ServerErrorResponse("Failed to get summaries.", ex);
        }

        // REPLY
        ctx.result(dict(
            data,
            "ID", request.getSeed(),
            "TAGS", tags,
            "SUMMARIES", summaries
        ));
    }

}

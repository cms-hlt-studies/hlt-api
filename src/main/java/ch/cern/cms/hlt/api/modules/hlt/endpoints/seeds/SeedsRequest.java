/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.seeds;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author omiguelc
 */
public class SeedsRequest {

    // CLASS
    private static final Pattern QUERY_PATTERN = Pattern.compile("(?:[\\w\\d_]+|\\*)+");

    // MEMBER
    private final int year;
    private final String query;
    private final String tag;

    public SeedsRequest(CERNClientContext ctx) {
        Map<String, List<String>> req = ctx.queryParamMap();

        this.year = dp()
            .get("year", Collections.emptyList())
            .asList(Integer.class)
            .get(0)
            .asInteger()
            .ifNull(2018)
            .apply(req);

        this.query = dp()
            .get("query", Collections.emptyList())
            .asList(String.class)
            .get(0)
            .asString()
            .check(query -> query == null || QUERY_PATTERN.matcher(query).matches(), "Invalid query.")
            .map(query -> {
                if (query == null) {
                    return null;
                }

                return query.replaceAll("\\*", "%");
            })
            .apply(req);

        this.tag = dp()
            .get("tag", Collections.emptyList())
            .asList(String.class)
            .get(0)
            .asString()
            .apply(req);
    }

    public int getYear() {
        return year;
    }

    public String getQuery() {
        return query;
    }

    public String getTag() {
        return tag;
    }

}

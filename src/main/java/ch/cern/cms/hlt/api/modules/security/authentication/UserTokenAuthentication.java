package ch.cern.cms.hlt.api.modules.security.authentication;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientProfile;
import com.smmx.analysis.quetzal.core.http.handlers.AuthenticationMethod;
import io.javalin.http.Context;

import static com.smmx.maria.commons.MariaCommons.vp;

public class UserTokenAuthentication implements AuthenticationMethod<CERNClientProfile> {

    // STATIC
    public static final String AUTHORIZATION_TYPE = "HLT-UTKN";

    // CLASS
    @Override
    public CERNClientProfile authenticate(Context ctx) {
        ////////////////////////////////////////////////////////////////////////
        // READ TOKEN //////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        String a_token = null;

        // READ CREDENTIALS
        String authorization_header = ctx.header("Authorization");

        if (authorization_header != null && authorization_header.startsWith(AUTHORIZATION_TYPE)) {
            a_token = authorization_header.trim()
                .replaceFirst(AUTHORIZATION_TYPE + " ", "");
        }

        a_token = vp()
            .asString()
            .apply(a_token);

        // SHORT-CIRCUIT
        if (a_token == null) {
            return null;
        }

        ////////////////////////////////////////////////////////////////////////
        // CREATE PROFILE //////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////

        // READ TOKEN
        UserToken token = new UserToken(a_token);
        ctx.header("Access-Control-Expose-Headers", AUTHORIZATION_TYPE);
        ctx.header(AUTHORIZATION_TYPE, token.sign());

        // CREATE PROFILE
        CERNClientProfile profile = new CERNClientProfile(false);

        profile.setSessionId(token.getTokenId());
        profile.setClientId(null);
        profile.setAppId(null);
        profile.setNICEAccountId(token.getNICEAccountId());
        profile.getScopes().addAll(token.getScopes());
        profile.getScopes().add(SecurityModule.ROLE_USER_TOKEN);

        return profile;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.endpoints.users_create;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNScopeRegistry;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Role;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.dp;
import static com.smmx.maria.commons.MariaCommons.vp;

/**
 * @author Osvaldo Miguel Colin
 */
public class UsersCreateRequest {

    private final String user;
    private final Set<Role> roles;

    public UsersCreateRequest(CERNClientContext ctx) {
        Map<String, Object> req = ctx.bodyParamMap();

        this.user = dp()
            .require("user")
            .asString()
            .notNull()
            .apply(req);

        this.roles = dp()
            .require("roles")
            .asList()
            .notNull()
            .check(l -> !l.isEmpty(), "At least one role must be granted.")
            .map(roles -> {
                return roles.stream()
                    .map(role -> {
                        return vp()
                            .asString()
                            .map(CERNScopeRegistry.getInstance()::resolveRole)
                            .apply(role);
                    })
                    .collect(Collectors.toSet());
            })
            .apply(req);
    }

    public String getUser() {
        return user;
    }

    public Set<Role> getRoles() {
        return roles;
    }
}

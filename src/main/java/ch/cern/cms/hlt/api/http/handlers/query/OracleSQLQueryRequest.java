/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.http.handlers.query;

import com.smmx.analysis.quetzal.core.http.handlers.ClientContext;

/**
 * @author Osvaldo Miguel Colin
 */
public class OracleSQLQueryRequest {

    private final OracleSQLSortRequest sort;
    private final OracleSQLPaginationRequest pagination;

    public OracleSQLQueryRequest(ClientContext ctx) {
        sort = new OracleSQLSortRequest(ctx.queryParamMap());
        pagination = new OracleSQLPaginationRequest(ctx.queryParamMap());
    }

    public OracleSQLSortRequest getQuerySort() {
        return sort;
    }

    public OracleSQLPaginationRequest getQueryPagination() {
        return pagination;
    }

}

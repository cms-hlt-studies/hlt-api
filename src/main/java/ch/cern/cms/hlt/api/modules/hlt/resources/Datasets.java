package ch.cern.cms.hlt.api.modules.hlt.resources;

import com.smmx.analysis.quetzal.core.http.errors.responses.ClientErrorResponse;
import com.smmx.analysis.quetzal.core.http.errors.responses.ServerErrorResponse;
import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionShield;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.smmx.analysis.quetzal.QuetzalUtils.readConnection;
import static com.smmx.analysis.quetzal.QuetzalUtils.sqlt;
import static com.smmx.maria.commons.MariaCommons.dict;

public class Datasets {

    public static byte[] resolveName(String name) {
        try (Loan<ConnectionShield> loan = readConnection()) {
            return (byte[]) sqlt("hlta::dataset::resolve")
                .preparedStatement(loan.getObject())
                .queryAndGetFirstValue(dict(
                    "NAME", name
                ))
                .orElseThrow(() -> new ClientErrorResponse("Dataset doesn't exist."));
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }
    }

    public static List<Map<String, Object>> getPathsForPeriod(byte[] dataset, Date from, Date to) {
        try (Loan<ConnectionShield> loan = readConnection()) {
            return sqlt("hlta::dataset::paths_period_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "DATASET", dataset,
                    "FROM", from,
                    "TO", to
                ));
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }
    }

}

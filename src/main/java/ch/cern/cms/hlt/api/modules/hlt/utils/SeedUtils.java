/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.utils;

import ch.cern.cms.hlt.api.modules.hlt.resources.Seeds;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import com.smmx.analysis.quetzal.core.http.errors.responses.ClientErrorResponse;
import com.smmx.maria.commons.optionals.OptionalDefinition;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;
import static com.smmx.maria.commons.MariaCommons.vp;

/**
 * @author omiguelc
 */
public class SeedUtils {

    public static byte[] seedFromQueryParams(CERNClientContext ctx) {
        Map<String, List<String>> query_params = ctx.queryParamMap();

        byte[] seed = dp()
            .get("seed", Collections.emptyList())
            .asList(String.class)
            .get(0)
            .asBinary()
            .apply(query_params);

        String seed_name = dp()
            .get("seed_name", Collections.emptyList())
            .asList(String.class)
            .get(0)
            .asString()
            .apply(query_params);

        return getSeedID(seed, seed_name);
    }

    public static OptionalDefinition<byte[]> optionalSeedFromQueryParams(CERNClientContext ctx) {
        Map<String, List<String>> query_params = ctx.queryParamMap();

        OptionalDefinition<byte[]> seed = dp()
            .optional(
                "seed", vp()
                    .asBinary()
            )
            .apply(query_params);

        OptionalDefinition<String> seed_name = dp()
            .optional(
                "seed_name", vp()
                    .asString()
            )
            .apply(query_params);

        if (!seed.isPresent() && !seed_name.isPresent()) {
            return OptionalDefinition.undefined();
        }

        return OptionalDefinition.of(
            getSeedID(
                seed.orElse(null),
                seed_name.orElse(null)
            )
        );
    }

    public static byte[] seedFromDictionary(Map<String, Object> params) {
        byte[] seed = dp()
            .get("seed")
            .asBinary()
            .apply(params);

        String seed_name = dp()
            .get("seed_name")
            .asString()
            .apply(params);

        return getSeedID(seed, seed_name);
    }

    public static OptionalDefinition<byte[]> optionalSeedFromDictionary(Map<String, Object> params) {
        OptionalDefinition<byte[]> seed = dp()
            .optional(
                "seed", vp()
                    .asBinary()
            )
            .apply(params);

        OptionalDefinition<String> seed_name = dp()
            .optional(
                "seed_name", vp()
                    .asString()
            )
            .apply(params);

        if (!seed.isPresent() && !seed_name.isPresent()) {
            return OptionalDefinition.undefined();
        }

        return OptionalDefinition.of(
            getSeedID(
                seed.orElse(null),
                seed_name.orElse(null)
            )
        );
    }

    public static byte[] getSeedID(byte[] seed, String seed_name) {
        if (seed == null && seed_name == null) {
            throw new ClientErrorResponse("seed or seed_name must be supplied.");
        }

        if (seed != null && seed_name != null) {
            throw new ClientErrorResponse("seed or seed_name must be supplied.");
        }

        if (seed != null) {
            return seed;
        }

        return Seeds.resolveName(seed_name);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.runs_summary;

import ch.cern.cms.hlt.api.modules.hlt.resources.Runs;
import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.AppAuthentication;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;

import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.apiSuccess;
import static com.smmx.maria.commons.MariaCommons.dict;

/**
 * @author omiguelc
 */
public class RunsSummaryHandler extends CERNClientHandler {

    public RunsSummaryHandler() {
        this.registerAuthenticationMethod(new AppAuthentication());
        this.permitScope(SecurityModule.PRIVILEGE_MINE);

        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_USER_TOKEN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        RunsSummaryRequest request = new RunsSummaryRequest(ctx);

        // GET DATA
        Map<String, Object> run_info = Runs.getInfo(request.getRunnumber());
        List<Map<String, Object>> paths = Runs.getPaths(request.getRunnumber());

        if (run_info == null) {
            ctx.result(apiSuccess());
            return;
        }

        // RESULT
        ctx.result(dict(
            run_info,
            "PATHS", paths
        ));
    }

}

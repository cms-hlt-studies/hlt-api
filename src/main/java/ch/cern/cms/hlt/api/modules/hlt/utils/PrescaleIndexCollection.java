package ch.cern.cms.hlt.api.modules.hlt.utils;

import java.util.ArrayList;
import java.util.List;

public class PrescaleIndexCollection {

    private PrescaleIndex previous_index;
    private final List<PrescaleIndex> indexes;
    private final List<Double> intlumi_list;
    private final List<Integer> index_list;
    private double intlumi;

    public PrescaleIndexCollection() {
        this.previous_index = null;
        this.indexes = new ArrayList<>();
        this.intlumi_list = new ArrayList<>();
        this.index_list = new ArrayList<>();
        this.intlumi = 0;
    }

    public void addInterval(int index_number, int runnumber, int[] interval, double intlumi) {
        // GET PRESCALE
        PrescaleIndex prescale = indexes.stream()
            .filter(a_prescale -> a_prescale.index == index_number)
            .findFirst()
            .orElse(null);

        // IF IT DOESN'T EXIST, CREATE IT
        if (prescale == null) {
            prescale = new PrescaleIndex(index_number);

            // APPEND
            indexes.add(prescale);
        }

        // ADD INTERVAL
        prescale.addInterval(runnumber, interval, intlumi);

        // CHANGE LOG
        if (previous_index == null || previous_index.index != index_number) {
            this.intlumi_list.add(this.intlumi);
            this.index_list.add(index_number);
        }

        // UPDATE PREVIOUS
        this.previous_index = prescale;

        // INTEGRATE LUMINOSITY
        this.intlumi += intlumi;
    }

    public List<PrescaleIndex> getIndexes() {
        return indexes;
    }

    public List<Double> getIntlumiChangeLog() {
        return intlumi_list;
    }

    public List<Integer> getIndexChangeLog() {
        return index_list;
    }

    public double getIntlumi() {
        return intlumi;
    }

    public class PrescaleIndex {

        private final int index;
        private Run previous_run;
        private final List<Run> runs;
        private double intlumi;

        public PrescaleIndex(int index) {
            this.index = index;
            this.previous_run = null;
            this.runs = new ArrayList<>();
            this.intlumi = 0;
        }

        public void addInterval(int runnumber, int[] interval, double intlumi) {
            // GET RUN
            Run run;

            if (previous_run == null || previous_run.runnumber != runnumber) {
                run = new Run(runnumber);

                // APPEND
                runs.add(run);
                previous_run = run;
            } else {
                run = previous_run;
            }

            // ADD INTERVAL
            run.addInterval(interval);

            // INTEGRATE LUMINOSITY
            this.intlumi += intlumi;
        }

        public int getIndex() {
            return index;
        }

        public List<Run> getRuns() {
            return runs;
        }

        public Run getRun(int runnumber) {
            return runs.stream()
                .filter(run -> run.getRunnumber() == runnumber)
                .findFirst()
                .orElse(null);
        }

        public double getIntlumi() {
            return intlumi;
        }

    }

    public class Run {

        private final int runnumber;
        private int[] previous_interval;
        private final List<int[]> intervals;

        public Run(int runnumber) {
            this.runnumber = runnumber;
            this.previous_interval = null;
            this.intervals = new ArrayList<>();
        }

        private void addInterval(int[] interval) {
            int n_intervals = intervals.size();

            if (previous_interval != null && previous_interval[1] == (interval[0] - 1)) {
                // THIS MEANS WE SHOULD UPDATE THE LAST INTERVAL
                this.intervals.get(n_intervals - 1)[1] = interval[1];
                this.previous_interval[1] = interval[1];
            } else {
                // THIS MEANS WE SHOULD APPEND IT
                intervals.add(interval);
                previous_interval = interval;
            }
        }

        public int getRunnumber() {
            return runnumber;
        }

        public List<int[]> getIntervals() {
            return intervals;
        }

    }

}

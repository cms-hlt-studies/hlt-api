package ch.cern.cms.hlt.api.modules.security.authentication;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.configuration.SecurityConfiguration;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientProfile;
import com.smmx.analysis.quetzal.core.http.handlers.AuthenticationMethod;
import com.smmx.analysis.quetzal.core.http.handlers.errors.UnauthorizedErrorResponse;
import com.smmx.analysis.quetzal.core.utils.Base64Utils;
import io.javalin.http.Context;

import java.util.Arrays;

import static com.smmx.maria.commons.MariaCommons.vp;

public class SSOAuthentication implements AuthenticationMethod<CERNClientProfile> {

    // STATIC
    public static final String AUTHORIZATION_TYPE = "HLT-USSO";

    // CLASS
    @Override
    public CERNClientProfile authenticate(Context ctx) {
        ////////////////////////////////////////////////////////////////////////
        // PARSE CREDENTIALS ///////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        String credentials_string = null;

        // READ CREDENTIALS
        String authorization_header = ctx.header("Authorization");

        if (authorization_header != null && authorization_header.startsWith(AUTHORIZATION_TYPE)) {
            credentials_string = authorization_header.trim()
                .replaceFirst(AUTHORIZATION_TYPE, "")
                .trim();
        }

        credentials_string = vp()
            .asString()
            .apply(credentials_string);

        // SHORT-CIRCUIT
        if (credentials_string == null) {
            return null;
        }

        // PARSE CREDENTIALS
        String[] credentials_parts = credentials_string.split(":", 2);

        String proxy_username = vp()
            .asString()
            .notNull()
            .apply(
                credentials_parts[0]
            );

        byte[] proxy_secret = vp()
            .asString()
            .notNull()
            .map(Base64Utils::urlDecodeNoPadding)
            .apply(
                credentials_parts[1]
            );

        ////////////////////////////////////////////////////////////////////////
        // AUTHENTICATE ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////

        // CHECK PROXY
        if (!Arrays.equals(proxy_secret, SecurityConfiguration.getInstance().getSSOSecret())) {
            throw new UnauthorizedErrorResponse();
        }

        ////////////////////////////////////////////////////////////////////////
        // CREATE PROFILE //////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        CERNClientProfile profile = new CERNClientProfile(false);

        profile.setClientId(null);
        profile.setAppId(null);
        profile.setNICEAccountId(proxy_username);
        profile.getScopes().add(SecurityModule.ROLE_USER_AUTHENTICATED);

        return profile;
    }

}

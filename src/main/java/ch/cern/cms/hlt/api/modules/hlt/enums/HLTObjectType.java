package ch.cern.cms.hlt.api.modules.hlt.enums;

public enum HLTObjectType {

    PATH, SEED

}

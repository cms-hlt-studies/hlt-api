/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api;

import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionShield;

import static com.smmx.analysis.quetzal.QuetzalUtils.connection;

/**
 * @author Osvaldo Miguel Colin
 */
public class HLTAPIUtils {

    public static final String SOURCE_READ_CONNECTION = "source-read";

    public static Loan<ConnectionShield> sourceReadConnection() {
        return connection(SOURCE_READ_CONNECTION);
    }
}

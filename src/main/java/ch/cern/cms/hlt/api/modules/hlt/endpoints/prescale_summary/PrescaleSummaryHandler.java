/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.prescale_summary;

import ch.cern.cms.hlt.api.modules.hlt.enums.HLTObjectType;
import ch.cern.cms.hlt.api.modules.hlt.resources.Paths;
import ch.cern.cms.hlt.api.modules.hlt.resources.Runs;
import ch.cern.cms.hlt.api.modules.hlt.resources.Seeds;
import ch.cern.cms.hlt.api.modules.hlt.utils.PrescaleCollection;
import ch.cern.cms.hlt.api.modules.hlt.utils.Prescales;
import ch.cern.cms.hlt.api.modules.hlt.utils.RunPrescaleMap;
import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.AppAuthentication;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;

import java.util.*;

import static com.smmx.maria.commons.MariaCommons.dict;
import static com.smmx.maria.commons.MariaCommons.ifnull;

/**
 * @author omiguelc
 */
public class PrescaleSummaryHandler extends CERNClientHandler {

    public PrescaleSummaryHandler() {
        this.registerAuthenticationMethod(new AppAuthentication());
        this.permitScope(SecurityModule.PRIVILEGE_MINE);

        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_USER_TOKEN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        PrescaleSummaryRequest request = new PrescaleSummaryRequest(ctx);

        // GET PATHS AND SEEDS
        List<Map<String, Object>> objects = new ArrayList<>();

        List<Map<String, Object>> period_prescale_indexes = Runs.getPrescaleIndexesForPeriod(
            request.getFrom(),
            request.getTo()
        );

        // BUILD OBJECT PRESCALE COLLECTIONS
        Map<String, Set<String>> run_objects = new HashMap<>();

        for (PrescaleSummaryRequest.HLTObject object : request.getObjects()) {
            String object_name;
            RunPrescaleMap run_prescale_map;

            if (HLTObjectType.PATH.equals(object.getType())) {
                object_name = object.getName();

                if (object_name != null) {
                    object_name = Paths.getName(object.getId());
                }

                run_prescale_map = Paths.getPrescalesForPeriod(
                    object.getId(),
                    request.getFrom(),
                    request.getTo()
                );
            } else {
                object_name = object.getName();

                if (object_name != null) {
                    object_name = Seeds.getName(object.getId());
                }

                run_prescale_map = Seeds.getPrescalesForPeriod(
                    object.getId(),
                    request.getFrom(),
                    request.getTo()
                );
            }

            PrescaleCollection object_prescale_collection = new PrescaleCollection();

            for (Map<String, Object> prescale_index_data : period_prescale_indexes) {
                int index = (int) prescale_index_data.get("INDEX");
                int runnumber = (int) prescale_index_data.get("RUNNUMBER");
                int from = (int) prescale_index_data.get("FROM");
                int to = (int) prescale_index_data.get("TO");
                double actual_intlumi = (double) prescale_index_data.get("INTLUMI");

                // GET PRESCALES
                Prescales prescales = run_prescale_map.get(runnumber);

                if (prescales == null) {
                    continue;
                }

                // ADD PATH
                String runnumber_key = Integer.toString(runnumber);

                if (!run_objects.containsKey(runnumber_key)) {
                    run_objects.put(runnumber_key, new HashSet<>());
                }

                run_objects.get(runnumber_key)
                    .add(object_name);

                // ADD PRESCALES
                double prescale = (prescales.size() <= index || index < 0) ? -1 : prescales.get(index);

                // ADD INTERVAL
                object_prescale_collection.addInterval(
                    prescale, runnumber, new int[]{from, to}, actual_intlumi
                );
            }

            // SHORT-CIRCUIT
            if (object_prescale_collection.getActualIntlumi() == 0) {
                continue;
            }

            // APPEND
            objects.add(dict(
                "NAME", object_name,
                "PRLS", object_prescale_collection.getPRLSList(),
                "CHANGE_LOG", dict(
                    "ACTUAL_INTLUMI", object_prescale_collection.getActualIntlumiChangeLog(),
                    "EFFECTIVE_INTLUMI", object_prescale_collection.getEffectiveIntlumi(),
                    "PRESCALE", object_prescale_collection.getPrescaleChangeLog()
                ),
                "ACTUAL_INTLUMI", object_prescale_collection.getActualIntlumi(),
                "EFFECTIVE_INTLUMI", object_prescale_collection.getEffectiveIntlumi()
            ));
        }

        // GET RUN INTLUMI
        List<Map<String, Object>> runs = new ArrayList<>();

        int current_runnumber = -1;
        double run_actual_intlumi_start = 0;
        double run_actual_intlumi_end = 0;

        for (Map<String, Object> prescale_index_data : period_prescale_indexes) {
            int runnumber = (int) prescale_index_data.get("RUNNUMBER");
            double actual_intlumi = (double) prescale_index_data.get("INTLUMI");

            if (current_runnumber != runnumber) {
                // PUSH
                if (current_runnumber != -1) {
                    Set<String> current_objects = run_objects.get(Integer.toString(current_runnumber));

                    runs.add(dict(
                        "RUNNUMBER", current_runnumber,
                        "INTLUMI_START", run_actual_intlumi_start,
                        "INTLUMI_END", run_actual_intlumi_end,
                        "OBJECTS", ifnull(current_objects, Collections.emptyList())
                    ));
                }

                // SWITCH
                current_runnumber = runnumber;
                run_actual_intlumi_start = run_actual_intlumi_end;
            }

            run_actual_intlumi_end += actual_intlumi;
        }

        // PUSH
        if (current_runnumber != -1) {
            Set<String> current_objects = run_objects.get(Integer.toString(current_runnumber));

            runs.add(dict(
                "RUNNUMBER", current_runnumber,
                "INTLUMI_START", run_actual_intlumi_start,
                "INTLUMI_END", run_actual_intlumi_end,
                "OBJECTS", ifnull(current_objects, Collections.emptyList())
            ));
        }

        // RESULT
        ctx.result(dict(
            "RUNS", runs,
            "OBJECTS", objects
        ));
    }

}

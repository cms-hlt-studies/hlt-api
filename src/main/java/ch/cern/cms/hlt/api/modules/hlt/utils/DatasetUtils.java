/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.utils;

import ch.cern.cms.hlt.api.modules.hlt.resources.Datasets;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import com.smmx.analysis.quetzal.core.http.errors.responses.ClientErrorResponse;
import com.smmx.maria.commons.optionals.OptionalDefinition;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;
import static com.smmx.maria.commons.MariaCommons.vp;

/**
 * @author omiguelc
 */
public class DatasetUtils {

    public static byte[] datasetFromQueryParams(CERNClientContext ctx) {
        Map<String, List<String>> query_params = ctx.queryParamMap();

        byte[] dataset = dp()
            .get("dataset", Collections.emptyList())
            .asList(String.class)
            .get(0)
            .asBinary()
            .apply(query_params);

        String dataset_name = dp()
            .get("dataset_name", Collections.emptyList())
            .asList(String.class)
            .get(0)
            .asString()
            .apply(query_params);

        return getDatasetID(dataset, dataset_name);
    }

    public static OptionalDefinition<byte[]> optionalDatasetFromQueryParams(CERNClientContext ctx) {
        Map<String, List<String>> query_params = ctx.queryParamMap();

        OptionalDefinition<byte[]> dataset = dp()
            .optional(
                "dataset", vp()
                    .asBinary()
            )
            .apply(query_params);

        OptionalDefinition<String> dataset_name = dp()
            .optional(
                "dataset_name", vp()
                    .asString()
            )
            .apply(query_params);

        if (!dataset.isPresent() && !dataset_name.isPresent()) {
            return OptionalDefinition.undefined();
        }

        return OptionalDefinition.of(
            getDatasetID(
                dataset.orElse(null),
                dataset_name.orElse(null)
            )
        );
    }

    public static byte[] datasetFromDictionary(Map<String, Object> params) {
        byte[] dataset = dp()
            .get("dataset")
            .asBinary()
            .apply(params);

        String dataset_name = dp()
            .get("dataset_name")
            .asString()
            .apply(params);

        return getDatasetID(dataset, dataset_name);
    }

    public static OptionalDefinition<byte[]> optionalDatasetFromDictionary(Map<String, Object> params) {
        OptionalDefinition<byte[]> dataset = dp()
            .optional(
                "dataset", vp()
                    .asBinary()
            )
            .apply(params);

        OptionalDefinition<String> dataset_name = dp()
            .optional(
                "dataset_name", vp()
                    .asString()
            )
            .apply(params);

        if (!dataset.isPresent() && !dataset_name.isPresent()) {
            return OptionalDefinition.undefined();
        }

        return OptionalDefinition.of(
            getDatasetID(
                dataset.orElse(null),
                dataset_name.orElse(null)
            )
        );
    }

    public static byte[] getDatasetID(byte[] dataset, String dataset_name) {
        if (dataset == null && dataset_name == null) {
            throw new ClientErrorResponse("dataset or dataset_name must be supplied.");
        }

        if (dataset != null && dataset_name != null) {
            throw new ClientErrorResponse("dataset or dataset_name must be supplied.");
        }

        if (dataset != null) {
            return dataset;
        }

        return Datasets.resolveName(dataset_name);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.endpoints.apps_create;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNScopeRegistry;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Privilege;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.dp;
import static com.smmx.maria.commons.MariaCommons.vp;

/**
 * @author Osvaldo Miguel Colin
 */
public class AppsCreateRequest {

    private final String name;
    private final String description;
    private final Set<Privilege> privileges;

    public AppsCreateRequest(CERNClientContext ctx) {
        Map<String, Object> req = ctx.bodyParamMap();

        this.name = dp()
            .require("name")
            .asString()
            .notNull()
            .apply(req);

        this.description = dp()
            .require("description")
            .asString()
            .notNull()
            .apply(req);

        this.privileges = dp()
            .require("privileges")
            .asList()
            .notNull()
            .check(l -> !l.isEmpty(), "At least one privilege must be granted.")
            .map(privileges -> {
                return privileges.stream()
                    .map(role -> {
                        return vp()
                            .asString()
                            .map(CERNScopeRegistry.getInstance()::resolvePrivilege)
                            .apply(role);
                    })
                    .collect(Collectors.toSet());
            })
            .apply(req);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<Privilege> getPrivileges() {
        return privileges;
    }
}

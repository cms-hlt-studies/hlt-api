package ch.cern.cms.hlt.api.modules.hlt.utils;

import java.util.HashMap;
import java.util.Map;

public class RunPrescaleMap {

    private final Map<String, Prescales> runs;

    public RunPrescaleMap() {
        runs = new HashMap<>();
    }

    public void put(int runnumber, Prescales prescales) {
        runs.put(Integer.toString(runnumber), prescales);
    }

    public Prescales get(int runnumber) {
        return runs.get(Integer.toString(runnumber));
    }

}

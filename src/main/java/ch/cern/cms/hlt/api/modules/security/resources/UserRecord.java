package ch.cern.cms.hlt.api.modules.security.resources;

import ch.cern.cms.hlt.api.modules.security.security.CERNScopeRegistry;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Role;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Scope;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.vp;

public class UserRecord {

    private String nice_account_id;
    private final Set<Role> roles;
    private Long timestamp;

    public UserRecord() {
        this.nice_account_id = null;
        this.roles = new HashSet<>();
        this.timestamp = null;
    }

    public UserRecord(String line) {
        String[] line_parts = line.split(":", 3);

        this.nice_account_id = vp()
            .asString()
            .notNull()
            .apply(line_parts[0]);

        this.roles = vp()
            .asString()
            .map(value -> {
                if (value == null) {
                    return Collections.<Role>emptySet();
                }

                return Arrays.stream(value.split(","))
                    .map(CERNScopeRegistry.getInstance()::resolveRole)
                    .collect(Collectors.toSet());
            })
            .apply(line_parts[1]);

        this.timestamp = vp()
            .asLong()
            .notNull()
            .apply(line_parts[2]);
    }

    public String sign() {
        return nice_account_id
            + ":" + roles.stream()
            .map(Scope::getName)
            .collect(Collectors.joining(","))
            + ":" + timestamp;
    }

    // GETTER AND SETTER
    public String getNICEAccountId() {
        return nice_account_id;
    }

    public void setNICEAccountId(String nice_account_id) {
        this.nice_account_id = nice_account_id;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles.clear();
        this.roles.addAll(roles);
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

}

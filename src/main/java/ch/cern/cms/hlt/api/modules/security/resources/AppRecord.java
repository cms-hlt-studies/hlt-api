package ch.cern.cms.hlt.api.modules.security.resources;

import ch.cern.cms.hlt.api.modules.security.security.CERNScopeRegistry;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Privilege;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Scope;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.vp;

public class AppRecord {

    private String id;
    private String name;
    private String description;
    private String secret_key;
    private final Set<Privilege> privileges;
    private Long timestamp;

    public AppRecord() {
        this.id = null;
        this.name = null;
        this.description = null;
        this.secret_key = null;
        this.privileges = new HashSet<>();
        this.timestamp = null;
    }

    public AppRecord(String line) {
        String[] line_parts = line.split(":", 6);

        this.id = vp()
            .asString()
            .notNull()
            .apply(line_parts[0]);

        this.name = vp()
            .asString()
            .notNull()
            .apply(line_parts[1]);

        this.description = vp()
            .asString()
            .notNull()
            .apply(line_parts[2]);

        this.secret_key = vp()
            .asString()
            .notNull()
            .apply(line_parts[3]);

        this.privileges = vp()
            .asString()
            .map(value -> {
                if (value == null) {
                    return Collections.<Privilege>emptySet();
                }

                return Arrays.stream(value.split(","))
                    .map(CERNScopeRegistry.getInstance()::resolvePrivilege)
                    .collect(Collectors.toSet());
            })
            .apply(line_parts[4]);

        this.timestamp = vp()
            .asLong()
            .notNull()
            .apply(line_parts[5]);
    }

    public String sign() {
        return id
            + ":" + name
            + ":" + description
            + ":" + secret_key
            + ":" + privileges.stream()
            .map(Scope::getName)
            .collect(Collectors.joining(","))
            + ":" + timestamp;
    }

    // GETTER AND SETTER
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSecretKey() {
        return secret_key;
    }

    public void setSecretKey(String secret_key) {
        this.secret_key = secret_key;
    }

    public Set<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Set<Privilege> privileges) {
        this.privileges.clear();
        this.privileges.addAll(privileges);
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

}

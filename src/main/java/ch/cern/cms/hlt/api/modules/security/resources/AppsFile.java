package ch.cern.cms.hlt.api.modules.security.resources;

import ch.cern.cms.hlt.api.modules.security.configuration.SecurityConfiguration;
import com.smmx.analysis.quetzal.core.http.errors.ServerException;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Privilege;
import com.smmx.analysis.quetzal.core.utils.Base64Utils;
import com.smmx.maria.commons.data.Tuple;
import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.smmx.maria.commons.MariaCommons.*;

public class AppsFile {

    // SINGLETON
    private static AppsFile INSTANCE;

    static {
        INSTANCE = null;
    }

    public static AppsFile getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AppsFile();
        }

        return INSTANCE;
    }

    // CLASS
    private AppsFile() {
        try {
            FileUtils.touch(
                SecurityConfiguration.getInstance()
                    .getApps()
                    .toFile()
            );
        } catch (IOException e) {
            throw new ServerException("Failed to touch file.");
        }
    }

    public List<AppRecord> getAllApps() {
        List<AppRecord> apps = new ArrayList<>();

        try (
            FileChannel channel = FileChannel.open(
                SecurityConfiguration.getInstance().getApps(),
                StandardOpenOption.READ
            );
            FileLock lock = channel.lock(0, Long.MAX_VALUE, true)
        ) {
            // READER
            BufferedReader reader = new BufferedReader(
                Channels.newReader(channel, StandardCharsets.UTF_8.name())
            );

            // ITERATE LINES
            String line;

            while ((line = reader.readLine()) != null) {
                // SHORT-CIRCUIT
                if (line.isEmpty()) {
                    continue;
                }

                // PARSE
                apps.add(new AppRecord(line));
            }
        } catch (IOException ex) {
            throw new ServerException("Failed to read apps file", ex);
        }

        return apps;
    }

    public AppRecord getApp(String app) {
        try (
            FileChannel channel = FileChannel.open(
                SecurityConfiguration.getInstance().getApps(),
                StandardOpenOption.READ
            );
            FileLock lock = channel.lock(0, Long.MAX_VALUE, true)
        ) {
            // READER
            BufferedReader reader = new BufferedReader(
                Channels.newReader(channel, StandardCharsets.UTF_8.name())
            );

            // ITERATE LINES
            String line;

            while ((line = reader.readLine()) != null) {
                // SHORT-CIRCUIT
                if (line.isEmpty()) {
                    continue;
                }

                // PARSE
                AppRecord record = new AppRecord(line);

                if (Objects.equals(record.getId(), app)) {
                    return record;
                }
            }
        } catch (IOException ex) {
            throw new ServerException("Failed to read apps file", ex);
        }

        return null;
    }

    public AppRecord getAppBySK(String secret_key) {
        try (
            FileChannel channel = FileChannel.open(
                SecurityConfiguration.getInstance().getApps(),
                StandardOpenOption.READ
            );
            FileLock lock = channel.lock(0, Long.MAX_VALUE, true)
        ) {
            // READER
            BufferedReader reader = new BufferedReader(
                Channels.newReader(channel, StandardCharsets.UTF_8.name())
            );

            // ITERATE LINES
            String line;

            while ((line = reader.readLine()) != null) {
                // SHORT-CIRCUIT
                if (line.isEmpty()) {
                    continue;
                }

                // PARSE
                AppRecord record = new AppRecord(line);

                if (Objects.equals(record.getSecretKey(), secret_key)) {
                    return record;
                }
            }
        } catch (IOException ex) {
            throw new ServerException("Failed to read apps file", ex);
        }

        return null;
    }

    public Tuple createApp(String name, String description, Set<Privilege> privileges) {
        try (
            FileChannel channel = FileChannel.open(
                SecurityConfiguration.getInstance().getApps(),
                StandardOpenOption.READ,
                StandardOpenOption.WRITE
            );
            FileLock lock = channel.lock(0, Long.MAX_VALUE, false)
        ) {
            // READER
            BufferedReader reader = new BufferedReader(
                Channels.newReader(channel, StandardCharsets.UTF_8.name())
            );

            // CHECK IT DOESN'T EXIST
            String line;

            while ((line = reader.readLine()) != null) {
                // SHORT-CIRCUIT
                if (line.isEmpty()) {
                    continue;
                }

                // PARSE
                AppRecord record = new AppRecord(line);

                if (Objects.equals(record.getName(), name)) {
                    throw new ServerException("App already exists.");
                }
            }

            // CREATE CREDENTIALS
            String app_id = Base64Utils.urlEncodeToStringNoPadding(
                buuid()
            );

            String app_secret = Base64Utils.urlEncodeToStringNoPadding(
                buuid()
            );

            // CREATE SERVICE
            AppRecord record = new AppRecord();

            record.setId(app_id);
            record.setName(name);
            record.setDescription(description);
            record.setSecretKey(app_secret);
            record.setPrivileges(privileges);
            record.setTimestamp(now().getTime() / 1000L);

            // BUILD LINE
            String append_line = record.sign() + System.lineSeparator();

            ByteBuffer byte_buffer = ByteBuffer.allocate(append_line.length());
            byte_buffer.put(append_line.getBytes(StandardCharsets.UTF_8));
            byte_buffer.flip(); // THIS SET'S IT TO OUTPUT MODE

            // WRITE
            channel.write(byte_buffer);

            // RETURN
            return tuple(app_id, app_secret);
        } catch (IOException ex) {
            throw new ServerException("Failed to read apps file", ex);
        }
    }

    public void deleteApp(String app) {
        try (
            FileChannel channel = FileChannel.open(
                SecurityConfiguration.getInstance().getApps(),
                StandardOpenOption.READ,
                StandardOpenOption.WRITE
            );
            FileLock lock = channel.lock(0, Long.MAX_VALUE, false)
        ) {
            // READER
            BufferedReader reader = new BufferedReader(
                Channels.newReader(channel, StandardCharsets.UTF_8.name())
            );

            // BUILDER
            StringBuilder lines_buffer = new StringBuilder();

            // ITERATE LINES
            String line;

            while ((line = reader.readLine()) != null) {
                // SHORT-CIRCUIT
                if (line.isEmpty()) {
                    continue;
                }

                // PARSE
                AppRecord record = new AppRecord(line);

                if (!Objects.equals(record.getId(), app)) {
                    lines_buffer.append(line + System.lineSeparator());
                }
            }

            // TRUNCATE
            channel.truncate(0);

            // GET BYTES
            String lines = lines_buffer.toString();
            ByteBuffer byte_buffer = ByteBuffer.allocate(lines.length());
            byte_buffer.put(lines.getBytes(StandardCharsets.UTF_8));
            byte_buffer.flip(); // THIS SET'S IT TO OUTPUT MODE

            // WRITE
            channel.write(byte_buffer);
        } catch (IOException ex) {
            throw new ServerException("Failed to read apps file", ex);
        }
    }

}

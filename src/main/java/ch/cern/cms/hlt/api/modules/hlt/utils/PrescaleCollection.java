package ch.cern.cms.hlt.api.modules.hlt.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dict;

public class PrescaleCollection {

    private Prescale previous_prescale;
    private final List<Prescale> prescales;
    private final List<Double> actual_intlumi_list;
    private final List<Double> effective_intlumi_list;
    private final List<Double> prescale_list;
    private double actual_intlumi;
    private double effective_intlumi;

    public PrescaleCollection() {
        this.previous_prescale = null;
        this.prescales = new ArrayList<>();
        this.actual_intlumi_list = new ArrayList<>();
        this.effective_intlumi_list = new ArrayList<>();
        this.prescale_list = new ArrayList<>();
        this.actual_intlumi = 0;
        this.effective_intlumi = 0;
    }

    public void addInterval(double prescale_value, int runnumber, int[] interval, double interval_actual_intlumi) {
        // NORMALIZE ACTUAL
        interval_actual_intlumi = (prescale_value > 0 ? interval_actual_intlumi : 0);

        // CALCULATE EFFECTIVE
        double interval_effective_intlumi = (prescale_value > 0 ? interval_actual_intlumi / prescale_value : 0);

        // GET PRESCALE
        Prescale prescale = prescales.stream()
            .filter(a_prescale -> a_prescale.value == prescale_value)
            .findFirst()
            .orElse(null);

        // IF IT DOESN'T EXIST, CREATE IT
        if (prescale == null) {
            prescale = new Prescale(prescale_value);

            // APPEND
            prescales.add(prescale);
        }

        // ADD INTERVAL
        prescale.addInterval(runnumber, interval, interval_actual_intlumi, interval_effective_intlumi);

        // CHANGE LOG
        if (previous_prescale == null || previous_prescale.value != prescale_value) {
            this.actual_intlumi_list.add(this.actual_intlumi);
            this.effective_intlumi_list.add(this.effective_intlumi);
            this.prescale_list.add(prescale_value);
        }

        // UPDATE PREVIOUS
        this.previous_prescale = prescale;

        // INTEGRATE LUMINOSITY
        this.actual_intlumi += interval_actual_intlumi;
        this.effective_intlumi += interval_effective_intlumi;
    }

    public List<Prescale> getPrescales() {
        return prescales;
    }

    public List<Double> getActualIntlumiChangeLog() {
        return actual_intlumi_list;
    }

    public List<Double> getEffectiveIntlumiChangeLog() {
        return effective_intlumi_list;
    }

    public List<Double> getPrescaleChangeLog() {
        return prescale_list;
    }

    public double getActualIntlumi() {
        return actual_intlumi;
    }

    public double getEffectiveIntlumi() {
        return effective_intlumi;
    }

    public List<Map<String, Object>> getPRLSList() {
        List<Map<String, Object>> prescale_list = new ArrayList<>();

        prescales.forEach(prescale -> {
            prescale_list.add(dict(
                "PRESCALE", prescale.getValue(),
                "RLS", prescale.toMap(),
                "ACTUAL_INTLUMI", prescale.getActualIntlumi(),
                "EFFECTIVE_INTLUMI", prescale.getEffectiveIntlumi()
            ));
        });

        return prescale_list;
    }

    public class Prescale {

        private final double value;
        private Run previous_run;
        private final List<Run> runs;
        private double actual_intlumi;
        private double effective_intlumi;

        public Prescale(double prescale) {
            this.value = prescale;
            this.previous_run = null;
            this.runs = new ArrayList<>();
            this.actual_intlumi = 0;
            this.effective_intlumi = 0;
        }

        public void addInterval(int runnumber, int[] interval, double actual_intlumi, double effective_intlumi) {
            // GET RUN
            Run run;

            if (previous_run == null || previous_run.runnumber != runnumber) {
                run = new Run(runnumber);

                // APPEND
                runs.add(run);
                previous_run = run;
            } else {
                run = previous_run;
            }

            // ADD INTERVAL
            run.addInterval(interval);

            // INTEGRATE LUMINOSITY
            this.actual_intlumi += actual_intlumi;
            this.effective_intlumi += effective_intlumi;
        }

        public double getValue() {
            return value;
        }

        public List<Run> getRuns() {
            return runs;
        }

        public Run getRun(int runnumber) {
            return runs.stream()
                .filter(run -> run.getRunnumber() == runnumber)
                .findFirst()
                .orElse(null);
        }

        public double getActualIntlumi() {
            return actual_intlumi;
        }

        public double getEffectiveIntlumi() {
            return effective_intlumi;
        }

        public Map<String, Object> toMap() {
            Map<String, Object> root = new HashMap<>();

            runs.forEach(run -> {
                root.put(
                    Integer.toString(run.runnumber),
                    run.getIntervals()
                );
            });

            return root;
        }

    }

    public class Run {

        private final int runnumber;
        private int[] previous_interval;
        private final List<int[]> intervals;

        public Run(int runnumber) {
            this.runnumber = runnumber;
            this.previous_interval = null;
            this.intervals = new ArrayList<>();
        }

        private void addInterval(int[] interval) {
            int n_intervals = intervals.size();

            if (previous_interval != null && previous_interval[1] == (interval[0] - 1)) {
                // THIS MEANS WE SHOULD UPDATE THE LAST INTERVAL
                this.intervals.get(n_intervals - 1)[1] = interval[1];
                this.previous_interval[1] = interval[1];
            } else {
                // THIS MEANS WE SHOULD APPEND IT
                intervals.add(interval);
                previous_interval = interval;
            }
        }

        public int getRunnumber() {
            return runnumber;
        }

        public List<int[]> getIntervals() {
            return intervals;
        }

    }

}

package ch.cern.cms.hlt.api;

import com.smmx.maria.commons.boot.AppRuntime;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Osvaldo Miguel Colin
 */
public class Main {

    public static void main(String[] args) {
        AppRuntime.getInstance()
            .headless()
            .timezone("UTC")
            .run(new HLTApi(), args);
    }

}

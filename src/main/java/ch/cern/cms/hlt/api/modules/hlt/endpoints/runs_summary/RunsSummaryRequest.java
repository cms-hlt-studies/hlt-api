/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.runs_summary;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author omiguelc
 */
public class RunsSummaryRequest {

    private final int runnumber;

    public RunsSummaryRequest(CERNClientContext ctx) {
        Map<String, List<String>> req = ctx.queryParamMap();

        this.runnumber = dp()
            .get("run", Collections.emptyList())
            .asList(String.class)
            .require(0)
            .asInteger()
            .notNull()
            .apply(req);
    }

    public int getRunnumber() {
        return runnumber;
    }

}

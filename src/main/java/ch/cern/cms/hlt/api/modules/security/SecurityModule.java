/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security;

import ch.cern.cms.hlt.api.HLTApiRuntime;
import ch.cern.cms.hlt.api.modules.security.datatypes.IdentifierDST;
import ch.cern.cms.hlt.api.modules.security.endpoints.apps.AppsHandler;
import ch.cern.cms.hlt.api.modules.security.endpoints.apps_create.AppsCreateHandler;
import ch.cern.cms.hlt.api.modules.security.endpoints.apps_delete.AppsDeleteHandler;
import ch.cern.cms.hlt.api.modules.security.endpoints.user_login.UserLoginHandler;
import ch.cern.cms.hlt.api.modules.security.endpoints.user_profile.UserProfileHandler;
import ch.cern.cms.hlt.api.modules.security.endpoints.users.UsersHandler;
import ch.cern.cms.hlt.api.modules.security.endpoints.users_create.UsersCreateHandler;
import ch.cern.cms.hlt.api.modules.security.endpoints.users_delete.UsersDeleteHandler;
import ch.cern.cms.hlt.api.modules.security.security.CERNScopeRegistry;
import com.smmx.analysis.quetzal.core.http.Http;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Privilege;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Role;
import com.smmx.maria.commons.boot.AppModule;
import com.smmx.maria.commons.values.subtypes.DSTManager;
import net.sourceforge.argparse4j.inf.ArgumentParser;

import java.util.Map;

/**
 * @author omiguelc
 */
public class SecurityModule extends AppModule {

    // SCOPES
    public static final Role ROLE_USER_AUTHENTICATED = CERNScopeRegistry.getInstance().registerRole("user-authenticated", true);
    public static final Role ROLE_USER_TOKEN = CERNScopeRegistry.getInstance().registerRole("user-token", true);

    public static final Role ROLE_ADMIN = CERNScopeRegistry.getInstance().registerRole("admin", false);
    public static final Role ROLE_MAINTAINER = CERNScopeRegistry.getInstance().registerRole("maintainer", false);

    public static final Role ROLE_APP = CERNScopeRegistry.getInstance().registerRole("app", true);

    public static final Privilege PRIVILEGE_MINE = CERNScopeRegistry.getInstance().registerPrivilege("mine");
    public static final Privilege PRIVILEGE_TAG = CERNScopeRegistry.getInstance().registerPrivilege("tag");

    // CLASS
    public SecurityModule() {
        super("SECURITY");
    }

    @Override
    public void configureArgumentParser(ArgumentParser parser) {
        // DO NOTHING
    }

    @Override
    public void configure(Map<String, Object> args) {
        DSTManager.getInstance().register(new IdentifierDST());
    }

    @Override
    public void setup() {
        if (HLTApiRuntime.setup_en) {
            setupDirectories();
            setupDatabase();
        } else {
            setupService();
        }
    }

    public void setupDirectories() {
        // DO NOTHING
    }

    public void setupDatabase() {
        // DO NOTHING
    }

    public void setupService() {
        Http.getInstance().get(
            "/cern/login",
            new UserLoginHandler()
        );
        Http.getInstance().get(
            "/security/user/profile",
            new UserProfileHandler()
        );

        Http.getInstance().get(
            "/security/users",
            new UsersHandler()
        );
        Http.getInstance().post(
            "/security/users",
            new UsersCreateHandler()
        );
        Http.getInstance().delete(
            "/security/users/:user",
            new UsersDeleteHandler()
        );

        Http.getInstance().get(
            "/security/apps",
            new AppsHandler()
        );
        Http.getInstance().post(
            "/security/apps",
            new AppsCreateHandler()
        );
        Http.getInstance().delete(
            "/security/apps/:app",
            new AppsDeleteHandler()
        );
    }

}

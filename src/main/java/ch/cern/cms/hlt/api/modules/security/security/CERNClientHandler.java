/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.security;

import com.smmx.analysis.quetzal.core.http.handlers.ClientHandler;
import io.javalin.http.Context;

/**
 * @author Osvaldo Miguel Colin
 */
public abstract class CERNClientHandler extends ClientHandler<CERNClientContext, CERNClientProfile> {

    @Override
    public CERNClientProfile createAnonymousProfile() {
        return new CERNClientProfile(true);
    }

    @Override
    public CERNClientContext createContext(Context ctx, CERNClientProfile profile) {
        return new CERNClientContext(ctx, profile);
    }

}

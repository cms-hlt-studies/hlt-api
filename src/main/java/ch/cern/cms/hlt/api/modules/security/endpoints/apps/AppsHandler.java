package ch.cern.cms.hlt.api.modules.security.endpoints.apps;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.resources.AppsFile;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Scope;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.dict;

public class AppsHandler extends CERNClientHandler {

    public AppsHandler() {
        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_ADMIN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // GET SERVICES
        List<Map<String, Object>> apps = AppsFile.getInstance().getAllApps()
            .stream()
            .map(record -> dict(
                "ID", record.getId(),
                "NAME", record.getName(),
                "DESCRIPTION", record.getDescription(),
                "SECRET_KEY", record.getSecretKey(),
                "PRIVILEGES", record.getPrivileges().stream()
                    .map(Scope::getName)
                    .collect(Collectors.toSet()),
                "TIMESTAMP", record.getTimestamp()
            ))
            .collect(Collectors.toList());

        // REPLY
        ctx.result(dict(
            "ENTRIES", apps
        ));
    }

}

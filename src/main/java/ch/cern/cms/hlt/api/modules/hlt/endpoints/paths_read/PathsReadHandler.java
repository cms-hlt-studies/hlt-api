/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.paths_read;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.AppAuthentication;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import com.smmx.analysis.quetzal.core.http.errors.ServerException;
import com.smmx.analysis.quetzal.core.http.errors.responses.ServerErrorResponse;
import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionShield;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.smmx.analysis.quetzal.QuetzalUtils.readConnection;
import static com.smmx.analysis.quetzal.QuetzalUtils.sqlt;
import static com.smmx.maria.commons.MariaCommons.dict;
import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class PathsReadHandler extends CERNClientHandler {

    public PathsReadHandler() {
        this.registerAuthenticationMethod(new AppAuthentication());
        this.permitScope(SecurityModule.PRIVILEGE_MINE);

        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_USER_TOKEN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // REQUEST
        PathsReadRequest request = new PathsReadRequest(ctx);

        // GET NAME
        Map<String, Object> data;

        try (Loan<ConnectionShield> loan = readConnection()) {
            data = sqlt("hlta::path::select")
                .preparedStatement(loan.getObject())
                .queryAndGetFirstMap(dict(
                    "PATH", request.getPath()
                ))
                .orElse(null);
        } catch (SQLException ex) {
            throw new ServerErrorResponse("Failed to get data.", ex);
        }

        // GET TAGS
        List<String> tags;

        try (Loan<ConnectionShield> loan = readConnection()) {
            tags = sqlt("hlta::path::tags_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "PATH", request.getPath()
                ))
                .stream()
                .map(dp().get("TAG").asString())
                .collect(Collectors.toList());
        } catch (SQLException ex) {
            throw new ServerException("Failed to get tags.", ex);
        }

        // GET YEARS
        List<Map<String, Object>> summaries;

        try (Loan<ConnectionShield> loan = readConnection()) {
            summaries = sqlt("hlta::path::summaries_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "PATH", request.getPath()
                ));
        } catch (SQLException ex) {
            throw new ServerErrorResponse("Failed to get summaries.", ex);
        }

        // GET SEEDS
        List<Map<String, Object>> seeds;

        try (Loan<ConnectionShield> loan = readConnection()) {
            seeds = sqlt("hlta::path::seeds_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "PATH", request.getPath()
                ));
        } catch (SQLException ex) {
            throw new ServerErrorResponse("Failed to get seeds.", ex);
        }

        // REPLY
        ctx.result(dict(
            data,
            "ID", request.getPath(),
            "TAGS", tags,
            "SUMMARIES", summaries,
            "SEEDS", seeds
        ));
    }

}

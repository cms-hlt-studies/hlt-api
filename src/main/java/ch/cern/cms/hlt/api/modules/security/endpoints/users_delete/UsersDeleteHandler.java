package ch.cern.cms.hlt.api.modules.security.endpoints.users_delete;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.resources.UsersFile;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;

import static com.smmx.maria.commons.MariaCommons.apiSuccess;

public class UsersDeleteHandler extends CERNClientHandler {

    public UsersDeleteHandler() {
        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_ADMIN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // REQUEST
        UsersDeleteRequest request = new UsersDeleteRequest(ctx);

        // REGISTER
        UsersFile.getInstance().deleteUser(
            request.getUser()
        );

        // REPLY
        ctx.result(apiSuccess());
    }

}

package ch.cern.cms.hlt.api.modules.hlt.resources;

import ch.cern.cms.hlt.api.modules.hlt.utils.Prescales;
import ch.cern.cms.hlt.api.modules.hlt.utils.RunPrescaleMap;
import com.smmx.analysis.quetzal.core.http.errors.responses.ClientErrorResponse;
import com.smmx.analysis.quetzal.core.http.errors.responses.ServerErrorResponse;
import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionShield;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.smmx.analysis.quetzal.QuetzalUtils.readConnection;
import static com.smmx.analysis.quetzal.QuetzalUtils.sqlt;
import static com.smmx.maria.commons.MariaCommons.dict;

public class Paths {

    public static byte[] resolveName(String name) {
        try (Loan<ConnectionShield> loan = readConnection()) {
            return (byte[]) sqlt("hlta::path::resolve")
                .preparedStatement(loan.getObject())
                .queryAndGetFirstValue(dict(
                    "NAME", name
                ))
                .orElseThrow(() -> new ClientErrorResponse("Path doesn't exist."));
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }
    }

    public static String getName(byte[] path) {
        try (Loan<ConnectionShield> loan = readConnection()) {
            return (String) sqlt("hlta::path::name")
                .preparedStatement(loan.getObject())
                .queryAndGetFirstValue(dict(
                    "PATH", path
                ))
                .orElseThrow(() -> new ClientErrorResponse("Path doesn't exist."));
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }
    }

    public static Map<String, Object> getInfo(int hltkey, byte[] path) {
        try (Loan<ConnectionShield> loan = readConnection()) {
            return sqlt("hlta::path::key_info_select")
                .preparedStatement(loan.getObject())
                .queryAndGetFirstMap(dict(
                    "HLTKEY", hltkey,
                    "PATH", path
                ))
                .orElse(null);
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }
    }

    public static RunPrescaleMap getPrescalesForPeriod(byte[] path, Date from, Date to) {
        List<Map<String, Object>> results;

        try (Loan<ConnectionShield> loan = readConnection()) {
            results = sqlt("hlta::path::prescales_period_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "PATH", path,
                    "FROM", from,
                    "TO", to
                ))
                .stream()
                .map(record -> {
                    // READ PRESCALES
                    Clob prescales_clob = ((Clob) record.get("PRESCALES"));

                    if (prescales_clob == null) {
                        return null;
                    }

                    String prescales;

                    try {
                        prescales = IOUtils.toString(prescales_clob.getCharacterStream());
                    } catch (IOException | SQLException ex) {
                        throw new ServerErrorResponse(ex);
                    }

                    // RETURN
                    return dict(
                        record,
                        "PRESCALES", prescales
                    );
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }

        RunPrescaleMap run_prescale_map = new RunPrescaleMap();

        results.forEach(result -> {
            int runnumber = (int) result.get("RUNNUMBER");
            Prescales prescales = new Prescales((String) result.get("PRESCALES"));

            run_prescale_map.put(runnumber, prescales);
        });

        return run_prescale_map;
    }

    public static List<Map<String, Object>> getSeedsForPeriod(byte[] path, Date from, Date to) {
        try (Loan<ConnectionShield> loan = readConnection()) {
            return sqlt("hlta::path::seeds_period_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "PATH", path,
                    "FROM", from,
                    "TO", to
                ));
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }
    }

}

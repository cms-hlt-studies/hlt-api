/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.http.handlers.query;

import com.smmx.analysis.quetzal.core.sql.builders.SQLField;
import com.smmx.maria.commons.database.sql.SQL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.*;

/**
 * @author Osvaldo Miguel Colin
 */
public class OracleSQLSortRequest {

    private final List<SortEntry> entries;

    public OracleSQLSortRequest(Map<String, List<String>> query) {
        this.entries = new ArrayList<>();

        List<String> field_values = dp()
            .get("_sFields", Collections.emptyList())
            .asList(String.class)
            .apply(query);

        field_values.forEach(value -> {
            // SHORT-CIRCUIT
            if (value.isEmpty()) {
                return;
            }

            // SPLIT
            String[] split_value = value.split(":");

            String field = vp()
                .asString()
                .notNull()
                .apply(split_value[0]);

            SortDirection direction = vp()
                .asEnum(SortDirection.class)
                .notNull()
                .apply(split_value[1]);

            this.entries.add(
                new SortEntry(field, direction)
            );
        });
    }

    public boolean isEmpty() {
        return this.entries.isEmpty();
    }

    public SQL toSQL(boolean include_order_by) {
        return toSQL(include_order_by, Collections.emptyList());
    }

    public SQL toSQL(boolean include_order_by, List<SQLField> fields) {
        if (entries.isEmpty()) {
            return sql("");
        }

        StringBuilder sb = new StringBuilder();

        if (include_order_by) {
            sb.append("ORDER BY ");
        }

        boolean is_first = true;
        boolean not_empty = true;

        for (SortEntry entry : entries) {
            if (is_first) {
                is_first = false;
            } else {
                sb.append(",");
            }

            String sql_field = fields.stream()
                .filter(f -> {
                    return f.getSQLName().toLowerCase()
                        .equals(entry.getField());
                })
                .findFirst()
                .map(SQLField::getSQLName)
                .orElse(null);

            if (sql_field == null) {
                continue;
            }

            String sql_direction = entry.getDirection().getSQLValue();

            sb.append("\"")
                .append(sql_field)
                .append("\" ")
                .append(sql_direction);

            not_empty = true;
        }

        if (not_empty) {
            return sql(sb.toString());
        }

        return sql("");
    }

    public enum SortDirection {
        ASC("ASC"), DESC("DESC");

        private final String sql;

        SortDirection(String sql_value) {
            this.sql = sql_value;
        }

        public String getSQLValue() {
            return sql;
        }
    }

    public class SortEntry {

        private final String field;
        private final SortDirection direction;

        private SortEntry(String field, SortDirection direction) {
            this.field = field;
            this.direction = direction;
        }

        public String getField() {
            return field;
        }

        public SortDirection getDirection() {
            return direction;
        }
    }

}

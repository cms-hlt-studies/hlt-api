/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt;

import ch.cern.cms.hlt.api.modules.hlt.endpoints.datasets.DatasetsHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.datasets_read.DatasetsReadHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.datasets_tags_add.DatasetsTagsAddHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.datasets_tags_remove.DatasetsTagsRemoveHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.datasets_update.DatasetsUpdateHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.paths.PathsHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.paths_read.PathsReadHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.paths_tags_add.PathsTagsAddHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.paths_tags_remove.PathsTagsRemoveHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.paths_update.PathsUpdateHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.prescale_summary.PrescaleSummaryHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.runs_summary.RunsSummaryHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.seeds.SeedsHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.seeds_read.SeedsReadHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.seeds_tags_add.SeedsTagsAddHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.seeds_tags_remove.SeedsTagsRemoveHandler;
import ch.cern.cms.hlt.api.modules.hlt.endpoints.seeds_update.SeedsUpdateHandler;
import com.smmx.analysis.quetzal.core.http.Http;
import com.smmx.maria.commons.boot.AppModule;
import net.sourceforge.argparse4j.inf.ArgumentParser;

import java.util.Map;

/**
 * @author Osvaldo Miguel Colin
 */
public class HLTModule extends AppModule {

    public HLTModule() {
        super("HLT");
    }

    @Override
    public void configureArgumentParser(ArgumentParser parser) {
        // DO NOTHING
    }

    @Override
    public void configure(Map<String, Object> args) {
        // DO NOTHING
    }

    @Override
    public void setup() {
        Http.getInstance().get("/cms/runs/summary", new RunsSummaryHandler());

        Http.getInstance().get("/hlt/datasets", new DatasetsHandler());
        Http.getInstance().get("/hlt/datasets/:dataset", new DatasetsReadHandler());
        Http.getInstance().put("/hlt/datasets/:dataset", new DatasetsUpdateHandler());
        Http.getInstance().post("/hlt/datasets/:dataset/tags", new DatasetsTagsAddHandler());
        Http.getInstance().delete("/hlt/datasets/:dataset/tags/:tag", new DatasetsTagsRemoveHandler());

        Http.getInstance().get("/hlt/paths", new PathsHandler());
        Http.getInstance().get("/hlt/paths/:path", new PathsReadHandler());
        Http.getInstance().put("/hlt/paths/:path", new PathsUpdateHandler());
        Http.getInstance().post("/hlt/paths/:path/tags", new PathsTagsAddHandler());
        Http.getInstance().delete("/hlt/paths/:path/tags/:tag", new PathsTagsRemoveHandler());

        Http.getInstance().get("/l1t/seeds", new SeedsHandler());
        Http.getInstance().get("/l1t/seeds/:seed", new SeedsReadHandler());
        Http.getInstance().put("/l1t/seeds/:seed", new SeedsUpdateHandler());
        Http.getInstance().post("/l1t/seeds/:seed/tags", new SeedsTagsAddHandler());
        Http.getInstance().delete("/l1t/seeds/:seed/tags/:tag", new SeedsTagsRemoveHandler());

        Http.getInstance().post("/hlt/tools/prescales", new PrescaleSummaryHandler());
    }

}

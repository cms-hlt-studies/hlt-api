/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.http.handlers.query;

import com.smmx.maria.commons.database.sql.SQL;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;
import static com.smmx.maria.commons.MariaCommons.sql;

/**
 * @author Osvaldo Miguel Colin
 */
public class OracleSQLPaginationRequest {

    public final int limit;
    public final int offset;

    public OracleSQLPaginationRequest(Map<String, List<String>> query) {
        this.limit = dp()
            .get("_pLimit", Collections.emptyList())
            .asList(String.class)
            .get(0, 50)
            .asInteger()
            .notNull()
            .apply(query);

        this.offset = dp()
            .get("_pOffset", Collections.emptyList())
            .asList(String.class)
            .get(0, 0)
            .asInteger()
            .notNull()
            .apply(query);
    }

    public SQL toSQL() {
        if (this.limit == -1) {
            return sql("");
        }

        return sql("OFFSET " + offset + " ROWS FETCH NEXT " + limit + " ROWS ONLY");
    }

}

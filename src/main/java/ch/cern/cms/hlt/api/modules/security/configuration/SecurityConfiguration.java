/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.configuration;

import com.smmx.analysis.quetzal.core.utils.Base64Utils;
import com.smmx.maria.commons.boot.AppConfiguration;
import com.smmx.maria.commons.boot.AppRuntime;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class SecurityConfiguration {

    // SINGLETON
    private static SecurityConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static SecurityConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SecurityConfiguration();
        }

        return INSTANCE;
    }

    // CLASS
    private final byte[] secret;
    private final byte[] sso_secret;
    private final Path users;
    private final Path apps;

    private SecurityConfiguration() {
        Map<String, Object> security_section = dp()
            .get("security", Collections.emptyMap())
            .asDictionary()
            .notNull()
            .apply(AppConfiguration.getInstance());

        this.secret = dp()
            .require("secret")
            .asString()
            .notNull()
            .map(Base64Utils::urlDecodeNoPadding)
            .apply(security_section);

        this.sso_secret = dp()
            .require("sso-secret")
            .asString()
            .notNull()
            .map(Base64Utils::urlDecodeNoPadding)
            .apply(security_section);

        this.users = dp()
            .require("users")
            .asString()
            .notNull()
            .map(users_path -> {
                if (users_path.startsWith("/")) {
                    return FileSystems.getDefault().getPath(
                        users_path
                    );
                }

                return AppRuntime.getInstance().home()
                    .resolve(users_path);
            })
            .apply(security_section);

        this.apps = dp()
            .require("apps")
            .asString()
            .notNull()
            .map(apps_path -> {
                if (apps_path.startsWith("/")) {
                    return FileSystems.getDefault().getPath(
                        apps_path
                    );
                }

                return AppRuntime.getInstance().home()
                    .resolve(apps_path);
            })
            .apply(security_section);
    }

    public byte[] getSecret() {
        return secret;
    }

    public byte[] getSSOSecret() {
        return sso_secret;
    }

    public Path getUsers() {
        return users;
    }

    public Path getApps() {
        return apps;
    }

}

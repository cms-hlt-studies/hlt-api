package ch.cern.cms.hlt.api.modules.hlt.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Prescales {

    private final Double[] values;

    public Prescales(String prescales) {
        // PARSE PRESCALES
        String[] parts = prescales.split(",");

        // UNPACK
        values = new Double[parts.length];

        for (int i = 0; i < parts.length; i++) {
            values[i] = Double.parseDouble(parts[i]);
        }
    }

    public List<Double> asList() {
        return Arrays.stream(values)
            .collect(Collectors.toList());
    }

    public int size() {
        return values.length;
    }

    public double get(int index) {
        return values[index];
    }

}

package ch.cern.cms.hlt.api.http.handlers.query;

import com.smmx.analysis.quetzal.core.http.errors.ServerException;
import com.smmx.analysis.quetzal.core.http.handlers.ClientContext;
import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.analysis.quetzal.core.sql.builders.SQLField;
import com.smmx.maria.commons.database.connections.ConnectionShield;
import com.smmx.maria.commons.database.sql.SQL;
import com.smmx.maria.commons.database.sql.SQLTemplate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.smmx.analysis.quetzal.QuetzalUtils.readConnection;

public class OracleSQLQueryUtils {

    public static List<Map<String, Object>> query(ClientContext ctx, SQLTemplate query_template, SQL select_snippet, Map<String, Object> select_args, SQL default_sort) {
        // GET SELECT FIELDS
        List<SQLField> select_fields = new ArrayList<>();

        query_template.getFields().entrySet().stream()
            .map(entry -> new SQLField(entry.getKey(), entry.getValue()))
            .forEach(select_fields::add);

        select_snippet.getFields().entrySet().stream()
            .map(entry -> new SQLField(entry.getKey(), entry.getValue()))
            .forEach(select_fields::add);

        // QUERY REQUEST
        OracleSQLQueryRequest query = new OracleSQLQueryRequest(ctx);
        OracleSQLSortRequest sort = query.getQuerySort();
        OracleSQLPaginationRequest pagination = query.getQueryPagination();

        // SORT
        SQL sort_snippet;

        if (sort.isEmpty()) {
            sort_snippet = default_sort;
        } else {
            sort_snippet = sort.toSQL(true, select_fields);
        }

        // PAGINATION
        SQL pagination_snippet = pagination.toSQL();

        // ENTRIES
        try (Loan<ConnectionShield> loan = readConnection()) {
            return query_template
                .define("__select__", select_snippet)
                .define("__order_by__", sort_snippet)
                .define("__pagination__", pagination_snippet)
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(select_args);
        } catch (SQLException ex) {
            throw new ServerException("Failed to get entries.", ex);
        }
    }
}

package ch.cern.cms.hlt.api.modules.security.endpoints.apps_delete;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.resources.AppsFile;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;

import static com.smmx.maria.commons.MariaCommons.apiSuccess;

public class AppsDeleteHandler extends CERNClientHandler {

    public AppsDeleteHandler() {
        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_ADMIN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // REQUEST
        AppsDeleteRequest request = new AppsDeleteRequest(ctx);

        // REGISTER
        AppsFile.getInstance().deleteApp(request.getApp());

        // REPLY
        ctx.result(apiSuccess());
    }

}

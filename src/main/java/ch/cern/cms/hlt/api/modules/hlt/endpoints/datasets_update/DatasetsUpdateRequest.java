/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.datasets_update;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;

import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class DatasetsUpdateRequest {

    private final byte[] dataset;
    private final String comments;

    public DatasetsUpdateRequest(CERNClientContext ctx) {
        Map<String, String> path = ctx.pathParamMap();

        this.dataset = dp()
            .require("dataset")
            .asBinary()
            .notNull()
            .apply(path);

        Map<String, Object> req = ctx.bodyParamMap();

        this.comments = dp()
            .require("comments")
            .asString()
            .apply(req);
    }

    public byte[] getDataset() {
        return dataset;
    }

    public String getComments() {
        return comments;
    }
}

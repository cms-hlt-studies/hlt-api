package ch.cern.cms.hlt.api.modules.security.authentication;

import ch.cern.cms.hlt.api.modules.security.configuration.SecurityConfiguration;
import ch.cern.cms.hlt.api.modules.security.security.CERNScopeRegistry;
import com.smmx.analysis.quetzal.core.http.handlers.errors.UnauthorizedErrorResponse;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Scope;
import com.smmx.analysis.quetzal.core.utils.Base64Utils;
import com.smmx.analysis.quetzal.core.utils.MACUtils;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.binToHex;
import static com.smmx.maria.commons.MariaCommons.vp;

public class UserToken {

    // STATIC
    private static final long TTL_SEC = 900; // 15 MIN

    // CLASS
    private byte[] token_id;
    private String nice_account_id;
    private Set<Scope> scopes;

    public UserToken() {
        token_id = null;
        nice_account_id = null;
        scopes = null;
    }

    public UserToken(String token) {
        String[] token_parts = token.split("\\.", 2);

        String token_message;
        String token_signature;

        try {
            token_message = vp()
                .asString()
                .notNull()
                .map(Base64Utils::urlDecodeNoPadding)
                .map(bytes -> new String(bytes, StandardCharsets.UTF_8))
                .apply(token_parts[0]);

            token_signature = vp()
                .asString()
                .notNull()
                .apply(token_parts[1]);
        } catch (Exception ex) {
            throw new UnauthorizedErrorResponse()
                .putDetail("solution", "login")
                .putDetail("reason", "invalid_token");
        }

        // VERIFY SIGNATURE
        String validation_signature = MACUtils.sign(
            token_message,
            SecurityConfiguration.getInstance().getSecret()
        );

        if (!Objects.equals(token_signature, validation_signature)) {
            throw new UnauthorizedErrorResponse()
                .putDetail("solution", "login")
                .putDetail("reason", "not_mine");
        }

        // UNPACK MESSAGE
        String[] token_message_parts = token_message.split(":", 4);

        try {
            token_id = vp()
                .asBinary()
                .notNull()
                .apply(token_message_parts[0]);

            nice_account_id = vp()
                .asString()
                .apply(token_message_parts[1]);

            scopes = vp()
                .asString()
                .notNull()
                .map(str -> str.split(","))
                .map(scope_names -> {
                    return Arrays.stream(scope_names)
                        .map(CERNScopeRegistry.getInstance()::resolveRole)
                        .map(role -> (Scope) role)
                        .collect(Collectors.toSet());
                })
                .apply(token_message_parts[2]);

            Long age = vp()
                .asLong()
                .notNull()
                .map(timestamp -> {
                    long now = System.currentTimeMillis() / 1000L;

                    // CALC
                    return now - timestamp;
                })
                .apply(token_message_parts[3]);

            if (age > TTL_SEC) {
                throw new UnauthorizedErrorResponse()
                    .putDetail("solution", "login")
                    .putDetail("reason", "expired");
            }
        } catch (Exception ex) {
            throw new UnauthorizedErrorResponse()
                .putDetail("solution", "login")
                .putDetail("reason", "invalid_token");
        }
    }

    // SIGN
    public String sign() {
        long timestamp = System.currentTimeMillis() / 1000L;

        String token_message = binToHex(token_id) // SESSION ID
            + ":" + (nice_account_id != null ? nice_account_id : "") // NICE ACCOUNT ID
            + ":" + scopes.stream().map(Scope::getName).collect(Collectors.joining(",")) // SCOPES
            + ":" + timestamp; // TIMESTAMP

        String token_message_b64 = Base64Utils.urlEncodeToStringNoPadding(
            token_message
        );

        String signature = MACUtils.sign(
            token_message,
            SecurityConfiguration.getInstance().getSecret()
        );

        return token_message_b64 + "." + signature;
    }

    // GETTER AND SETTER
    public byte[] getTokenId() {
        return token_id;
    }

    public void setTokenId(byte[] session_id) {
        this.token_id = session_id;
    }

    public String getNICEAccountId() {
        return nice_account_id;
    }

    public void setNICEAccountId(String nice_account_id) {
        this.nice_account_id = nice_account_id;
    }

    public Set<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(Set<Scope> scopes) {
        this.scopes = scopes;
    }
}

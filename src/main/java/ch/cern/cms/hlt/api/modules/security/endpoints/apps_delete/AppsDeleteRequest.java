/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.endpoints.apps_delete;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;

import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class AppsDeleteRequest {

    private final String app;

    public AppsDeleteRequest(CERNClientContext ctx) {
        Map<String, String> path = ctx.pathParamMap();

        this.app = dp()
            .require("app")
            .asString()
            .notNull()
            .apply(path);
    }

    public String getApp() {
        return app;
    }

}

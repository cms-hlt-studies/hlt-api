package ch.cern.cms.hlt.api.modules.security.endpoints.users_create;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.resources.UsersFile;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;

import static com.smmx.maria.commons.MariaCommons.apiSuccess;

public class UsersCreateHandler extends CERNClientHandler {

    public UsersCreateHandler() {
        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_ADMIN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // REQUEST
        UsersCreateRequest request = new UsersCreateRequest(ctx);

        // REGISTER
        UsersFile.getInstance().registerUser(
            request.getUser(),
            request.getRoles()
        );

        // REPLY
        ctx.result(apiSuccess());
    }

}

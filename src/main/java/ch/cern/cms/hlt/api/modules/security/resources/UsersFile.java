package ch.cern.cms.hlt.api.modules.security.resources;

import ch.cern.cms.hlt.api.modules.security.configuration.SecurityConfiguration;
import com.smmx.analysis.quetzal.core.http.errors.ServerException;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Role;
import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.smmx.maria.commons.MariaCommons.now;

public class UsersFile {

    // SINGLETON
    private static UsersFile INSTANCE;

    static {
        INSTANCE = null;
    }

    public static UsersFile getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UsersFile();
        }

        return INSTANCE;
    }

    // CLASS
    private UsersFile() {
        try {
            FileUtils.touch(
                SecurityConfiguration.getInstance()
                    .getUsers()
                    .toFile()
            );
        } catch (IOException e) {
            throw new ServerException("Failed to touch file.");
        }
    }

    public List<UserRecord> getAllUsers() {
        List<UserRecord> users = new ArrayList<>();

        try (
            FileChannel channel = FileChannel.open(
                SecurityConfiguration.getInstance().getUsers(),
                StandardOpenOption.READ
            );
            FileLock lock = channel.lock(0, Long.MAX_VALUE, true)
        ) {
            // READER
            BufferedReader reader = new BufferedReader(
                Channels.newReader(channel, StandardCharsets.UTF_8.name())
            );

            // ITERATE LINES
            String line;

            while ((line = reader.readLine()) != null) {
                // SHORT-CIRCUIT
                if (line.isEmpty()) {
                    continue;
                }

                // PARSE
                users.add(new UserRecord(line));
            }
        } catch (IOException ex) {
            throw new ServerException("Failed to read users file", ex);
        }

        return users;
    }

    public UserRecord getUser(String user) {
        try (
            FileChannel channel = FileChannel.open(
                SecurityConfiguration.getInstance().getUsers(),
                StandardOpenOption.READ
            );
            FileLock lock = channel.lock(0, Long.MAX_VALUE, true)
        ) {
            // READER
            BufferedReader reader = new BufferedReader(
                Channels.newReader(channel, StandardCharsets.UTF_8.name())
            );

            // ITERATE LINES
            String line;

            while ((line = reader.readLine()) != null) {
                // SHORT-CIRCUIT
                if (line.isEmpty()) {
                    continue;
                }

                // PARSE
                UserRecord record = new UserRecord(line);

                if (Objects.equals(record.getNICEAccountId(), user)) {
                    return record;
                }
            }
        } catch (IOException ex) {
            throw new ServerException("Failed to read users file", ex);
        }

        return null;
    }

    public void registerUser(String user, Set<Role> roles) {
        try (
            FileChannel channel = FileChannel.open(
                SecurityConfiguration.getInstance().getUsers(),
                StandardOpenOption.READ,
                StandardOpenOption.WRITE
            );
            FileLock lock = channel.lock(0, Long.MAX_VALUE, false)
        ) {
            // READER
            BufferedReader reader = new BufferedReader(
                Channels.newReader(channel, StandardCharsets.UTF_8.name())
            );

            // CHECK IT DOESN'T EXIST
            String line;

            while ((line = reader.readLine()) != null) {
                // SHORT-CIRCUIT
                if (line.isEmpty()) {
                    continue;
                }

                // PARSE
                UserRecord record = new UserRecord(line);

                if (Objects.equals(record.getNICEAccountId(), user)) {
                    throw new ServerException("Admin already exists.");
                }
            }

            // CREATE SERVICE
            UserRecord record = new UserRecord();

            record.setNICEAccountId(user);
            record.setRoles(roles);
            record.setTimestamp(now().getTime() / 1000L);

            // BUILD LINE
            String append_line = record.sign() + System.lineSeparator();

            ByteBuffer byte_buffer = ByteBuffer.allocate(append_line.length());
            byte_buffer.put(append_line.getBytes(StandardCharsets.UTF_8));
            byte_buffer.flip(); // THIS SET'S IT TO OUTPUT MODE

            // WRITE
            channel.write(byte_buffer);
        } catch (IOException ex) {
            throw new ServerException("Failed to read users file", ex);
        }
    }

    public void deleteUser(String user) {
        try (
            FileChannel channel = FileChannel.open(
                SecurityConfiguration.getInstance().getUsers(),
                StandardOpenOption.READ,
                StandardOpenOption.WRITE
            );
            FileLock lock = channel.lock(0, Long.MAX_VALUE, false)
        ) {
            // READER
            BufferedReader reader = new BufferedReader(
                Channels.newReader(channel, StandardCharsets.UTF_8.name())
            );

            // BUILDER
            StringBuilder lines_buffer = new StringBuilder();

            // ITERATE LINES
            String line;

            while ((line = reader.readLine()) != null) {
                // SHORT-CIRCUIT
                if (line.isEmpty()) {
                    continue;
                }

                // PARSE
                UserRecord record = new UserRecord(line);

                if (!Objects.equals(record.getNICEAccountId(), user)) {
                    lines_buffer.append(line + System.lineSeparator());
                }
            }

            // TRUNCATE
            channel.truncate(0);

            // GET BYTES
            String lines = lines_buffer.toString();
            ByteBuffer byte_buffer = ByteBuffer.allocate(lines.length());
            byte_buffer.put(lines.getBytes(StandardCharsets.UTF_8));
            byte_buffer.flip(); // THIS SET'S IT TO OUTPUT MODE

            // WRITE
            channel.write(byte_buffer);
        } catch (IOException ex) {
            throw new ServerException("Failed to read users file", ex);
        }
    }

}

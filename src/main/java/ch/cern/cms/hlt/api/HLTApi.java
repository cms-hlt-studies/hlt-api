/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api;

import ch.cern.cms.hlt.api.modules.hlt.HLTModule;
import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import com.smmx.analysis.quetzal.QuetzalApp;
import com.smmx.maria.commons.boot.AppConfigurationException;
import net.sourceforge.argparse4j.inf.ArgumentParser;

import java.util.Map;

/**
 * @author Osvaldo Miguel Colin
 */
public class HLTApi extends QuetzalApp {

    public HLTApi() {
        super(
            "HLT Api",
            "HLT Api Service"
        );

        this.addModule(new SecurityModule());
        this.addModule(new HLTModule());
    }

    @Override
    public void configureArgumentParser(ArgumentParser parser) {
        // ADD RUNTIME OPTIONS
        parser.addArgument("--debug-en")
            .metavar("Debug")
            .help("Runs service in debug mode.")
            .dest("debug_en")
            .type(Boolean.class)
            .setDefault(false)
            .setConst(true)
            .nargs("?");

        parser.addArgument("--setup-en")
            .metavar("Setup")
            .help("Runs the setup process and finishes the run.")
            .dest("setup_en")
            .type(Boolean.class)
            .setDefault(false)
            .setConst(true)
            .nargs("?");

        // MODULES
        super.configureArgumentParser(parser);
    }

    @Override
    public void configure(Map<String, Object> args) throws AppConfigurationException {
        HLTApiRuntime.debug_en = (boolean) args.get("debug_en");
        HLTApiRuntime.setup_en = (boolean) args.get("setup_en");

        // MODULES
        super.configure(args);
    }

    @Override
    public void start() {
        if (HLTApiRuntime.setup_en) {
            System.exit(0);
        } else {
            super.start();
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.datasets_tags_remove;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;

import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class DatasetsTagsRemoveRequest {

    private final byte[] dataset;
    private final String tag;

    public DatasetsTagsRemoveRequest(CERNClientContext ctx) {
        Map<String, String> path = ctx.pathParamMap();

        this.dataset = dp()
            .require("dataset")
            .asBinary()
            .notNull()
            .apply(path);

        this.tag = dp()
            .require("tag")
            .asString()
            .notNull()
            .apply(path);
    }

    public byte[] getDataset() {
        return dataset;
    }

    public String getTag() {
        return tag;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.security;

import com.smmx.analysis.quetzal.core.http.handlers.ClientProfile;

/**
 * @author Osvaldo Miguel Colin
 */
public class CERNClientProfile extends ClientProfile {

    private String app_id;
    private String nice_account_id;

    public CERNClientProfile(boolean is_anonymous) {
        super(is_anonymous);
    }

    public String getAppId() {
        return app_id;
    }

    public void setAppId(String app_id) {
        this.app_id = app_id;
    }

    public String getNICEAccountId() {
        return nice_account_id;
    }

    public void setNICEAccountId(String nice_account_id) {
        this.nice_account_id = nice_account_id;
    }
}

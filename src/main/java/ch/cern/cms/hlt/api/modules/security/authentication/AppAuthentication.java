package ch.cern.cms.hlt.api.modules.security.authentication;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.resources.AppRecord;
import ch.cern.cms.hlt.api.modules.security.resources.AppsFile;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientProfile;
import com.smmx.analysis.quetzal.core.http.handlers.AuthenticationMethod;
import com.smmx.analysis.quetzal.core.http.handlers.errors.UnauthorizedErrorResponse;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Privilege;
import io.javalin.http.Context;

import java.util.Set;

import static com.smmx.maria.commons.MariaCommons.vp;

public class AppAuthentication implements AuthenticationMethod<CERNClientProfile> {

    // STATIC
    public static final String AUTHORIZATION_TYPE = "HLT-APP";

    // CLASS
    @Override
    public CERNClientProfile authenticate(Context ctx) {
        ////////////////////////////////////////////////////////////////////////
        // READ TOKEN //////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        String credentials_string = null;

        // READ TOKEN
        String authorization_header = ctx.header("Authorization");

        if (authorization_header != null && authorization_header.startsWith(AUTHORIZATION_TYPE)) {
            credentials_string = authorization_header.trim()
                .replaceFirst(AUTHORIZATION_TYPE, "")
                .trim();
        }

        credentials_string = vp()
            .asString()
            .apply(credentials_string);

        // SHORT-CIRCUIT
        if (credentials_string == null) {
            return null;
        }

        // PARSE VALUES
        String secret_key = vp()
            .asString()
            .notNull()
            .apply(
                credentials_string
            );

        ////////////////////////////////////////////////////////////////////////
        // GET SERVICE /////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        AppRecord app = AppsFile.getInstance()
            .getAppBySK(secret_key);

        if (app == null) {
            throw new UnauthorizedErrorResponse();
        }

        Set<Privilege> app_privileges = app.getPrivileges();

        ////////////////////////////////////////////////////////////////////////
        // CREATE PROFILE //////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        CERNClientProfile profile = new CERNClientProfile(false);

        profile.setClientId(null);
        profile.setAppId(app.getId());
        profile.setNICEAccountId(null);
        profile.getScopes().addAll(app_privileges);
        profile.getScopes().add(SecurityModule.ROLE_APP);

        return profile;
    }

}

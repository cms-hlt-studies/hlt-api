/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.seeds_tags_remove;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;

import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class SeedsTagsRemoveRequest {

    private final byte[] seed;
    private final String tag;

    public SeedsTagsRemoveRequest(CERNClientContext ctx) {
        Map<String, String> path = ctx.pathParamMap();

        this.seed = dp()
            .require("seed")
            .asBinary()
            .notNull()
            .apply(path);

        this.tag = dp()
            .require("tag")
            .asString()
            .notNull()
            .apply(path);
    }

    public byte[] getSeed() {
        return seed;
    }

    public String getTag() {
        return tag;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.datatypes;

import com.smmx.maria.commons.values.errors.ValueError;
import com.smmx.maria.commons.values.subtypes.DST;

import java.util.regex.Pattern;

/**
 * @author omiguelc
 */
public class IdentifierDST extends DST<String> {

    private static final Pattern IDENTIFIER_PATTERN = Pattern.compile("[a-zA-Z0-9_]+");

    public IdentifierDST() {
        super("common::identifier");
    }

    @Override
    protected String valueOf(String value) {
        // SHORT-CIRCUIT
        if (value == null) {
            return null;
        }

        // VALIDATE
        boolean valid = IDENTIFIER_PATTERN.matcher(value).matches();

        if (!valid) {
            throw new ValueError("value is not a valid identfier.");
        }

        // RETURN
        return value;
    }

}

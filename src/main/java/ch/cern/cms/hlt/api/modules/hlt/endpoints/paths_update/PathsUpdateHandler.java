/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.endpoints.paths_update;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import com.smmx.analysis.quetzal.core.http.errors.ServerException;
import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionShield;
import com.smmx.maria.commons.database.transactions.Transaction;

import java.sql.SQLException;

import static com.smmx.analysis.quetzal.QuetzalUtils.sqlt;
import static com.smmx.analysis.quetzal.QuetzalUtils.writeConnection;
import static com.smmx.maria.commons.MariaCommons.*;

/**
 * @author Osvaldo Miguel Colin
 */
public class PathsUpdateHandler extends CERNClientHandler {

    public PathsUpdateHandler() {
        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_MAINTAINER);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // REQUEST
        PathsUpdateRequest request = new PathsUpdateRequest(ctx);

        // UPDATE
        Transaction transaction = transaction((connection) -> {
            sqlt("hlta::path::update")
                .preparedStatement(connection)
                .update(dict(
                    "PATH", request.getPath(),
                    "COMMENTS", request.getComments(),
                    "TIMESTAMP", now()
                ));
        });

        try (Loan<ConnectionShield> loan = writeConnection()) {
            transaction.run(loan.getObject());
        } catch (SQLException ex) {
            throw new ServerException("Failed to update path.", ex);
        }

        // REPLY
        ctx.result(apiSuccess());
    }

}

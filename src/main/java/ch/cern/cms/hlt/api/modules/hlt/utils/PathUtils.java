/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.hlt.utils;

import ch.cern.cms.hlt.api.modules.hlt.resources.Paths;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import com.smmx.analysis.quetzal.core.http.errors.responses.ClientErrorResponse;
import com.smmx.maria.commons.optionals.OptionalDefinition;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;
import static com.smmx.maria.commons.MariaCommons.vp;

/**
 * @author omiguelc
 */
public class PathUtils {

    public static byte[] pathFromQueryParams(CERNClientContext ctx) {
        Map<String, List<String>> query_params = ctx.queryParamMap();

        byte[] path = dp()
            .get("path", Collections.emptyList())
            .asList(String.class)
            .get(0)
            .asBinary()
            .apply(query_params);

        String path_name = dp()
            .get("path_name", Collections.emptyList())
            .asList(String.class)
            .get(0)
            .asString()
            .apply(query_params);

        return getPathID(path, path_name);
    }

    public static OptionalDefinition<byte[]> optionalPathFromQueryParams(CERNClientContext ctx) {
        Map<String, List<String>> query_params = ctx.queryParamMap();

        OptionalDefinition<byte[]> path = dp()
            .optional(
                "path", vp()
                    .asBinary()
            )
            .apply(query_params);

        OptionalDefinition<String> path_name = dp()
            .optional(
                "path_name", vp()
                    .asString()
            )
            .apply(query_params);

        if (!path.isPresent() && !path_name.isPresent()) {
            return OptionalDefinition.undefined();
        }

        return OptionalDefinition.of(
            getPathID(
                path.orElse(null),
                path_name.orElse(null)
            )
        );
    }

    public static byte[] pathFromDictionary(Map<String, Object> params) {
        byte[] path = dp()
            .get("path")
            .asBinary()
            .apply(params);

        String path_name = dp()
            .get("path_name")
            .asString()
            .apply(params);

        return getPathID(path, path_name);
    }

    public static OptionalDefinition<byte[]> optionalPathFromDictionary(Map<String, Object> params) {
        OptionalDefinition<byte[]> path = dp()
            .optional(
                "path", vp()
                    .asBinary()
            )
            .apply(params);

        OptionalDefinition<String> path_name = dp()
            .optional(
                "path_name", vp()
                    .asString()
            )
            .apply(params);

        if (!path.isPresent() && !path_name.isPresent()) {
            return OptionalDefinition.undefined();
        }

        return OptionalDefinition.of(
            getPathID(
                path.orElse(null),
                path_name.orElse(null)
            )
        );
    }

    public static byte[] getPathID(byte[] path, String path_name) {
        if (path == null && path_name == null) {
            throw new ClientErrorResponse("path or path_name must be supplied.");
        }

        if (path != null && path_name != null) {
            throw new ClientErrorResponse("path or path_name must be supplied.");
        }

        if (path != null) {
            return path;
        }

        return Paths.resolveName(path_name);
    }

}

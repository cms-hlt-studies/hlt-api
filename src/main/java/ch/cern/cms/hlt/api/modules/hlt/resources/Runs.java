package ch.cern.cms.hlt.api.modules.hlt.resources;

import com.smmx.analysis.quetzal.core.http.errors.responses.ServerErrorResponse;
import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionShield;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.smmx.analysis.quetzal.QuetzalUtils.readConnection;
import static com.smmx.analysis.quetzal.QuetzalUtils.sqlt;
import static com.smmx.maria.commons.MariaCommons.dict;

public class Runs {

    public static Map<String, Object> getInfo(int runnumber) {
        try (Loan<ConnectionShield> loan = readConnection()) {
            return sqlt("hlta::run::info_select")
                .preparedStatement(loan.getObject())
                .queryAndGetFirstMap(dict(
                    "RUNNUMBER", runnumber
                ))
                .orElse(null);
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }
    }

    public static List<Map<String, Object>> getPrescaleIndexesForPeriod(Date starttime, Date stoptime) {
        try (Loan<ConnectionShield> loan = readConnection()) {
            return sqlt("hlta::run::prescale_indexes_period_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "FROM", starttime,
                    "TO", stoptime
                ));
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }
    }

    public static List<Map<String, Object>> getPaths(int runnumber) {
        try (Loan<ConnectionShield> loan = readConnection()) {
            return sqlt("hlta::run::path_key_info_select")
                .preparedStatement(loan.getObject())
                .queryAndGetMaps(dict(
                    "RUNNUMBER", runnumber
                ));
        } catch (SQLException ex) {
            throw new ServerErrorResponse(ex);
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.endpoints.user_login;

import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;

import java.util.List;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class UserLoginRequest {

    private final String callback;

    public UserLoginRequest(CERNClientContext ctx) {
        Map<String, List<String>> req = ctx.queryParamMap();

        this.callback = dp()
            .require("callback")
            .asList(String.class)
            .require(0)
            .asString()
            .notNull()
            .apply(req);
    }

    public String getCallback() {
        return callback;
    }

}

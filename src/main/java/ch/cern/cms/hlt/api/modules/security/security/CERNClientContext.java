package ch.cern.cms.hlt.api.modules.security.security;

import com.smmx.analysis.quetzal.core.http.handlers.ClientContext;
import io.javalin.http.Context;

public class CERNClientContext extends ClientContext<CERNClientProfile> {

    public CERNClientContext(Context ctx, CERNClientProfile profile) {
        super(ctx, profile);
    }

}

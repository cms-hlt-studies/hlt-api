/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.endpoints.user_profile;


import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientProfile;
import com.smmx.analysis.quetzal.core.http.handlers.ClientScopes;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Scope;

import java.util.Collections;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.*;

/**
 * @author Osvaldo Miguel Colin
 */
public class UserProfileHandler extends CERNClientHandler {

    public UserProfileHandler() {
        // SECURITY
        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(ClientScopes.ROLE_ANYONE);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // PROFILE
        CERNClientProfile profile = ctx.getProfile();

        // HANDLE
        if (profile.isAnonymous()) {
            ctx.result(
                apiSuccess(dict(
                    "session-id", null,
                    "user-id", null,
                    "roles", Collections.emptyList()
                ))
            );
        } else {
            ctx.result(
                apiSuccess(dict(
                    "session-id", binToHex(profile.getSessionId()),
                    "user-id", profile.getClientId(),
                    "roles", profile.getScopes().stream()
                        .map(Scope::getName)
                        .sorted()
                        .collect(Collectors.toSet())
                ))
            );
        }
    }

}

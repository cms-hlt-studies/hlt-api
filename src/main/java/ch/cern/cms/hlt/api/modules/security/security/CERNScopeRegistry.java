/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.hlt.api.modules.security.security;

import com.smmx.analysis.quetzal.core.http.handlers.scopes.ScopeRegistry;

/**
 * @author Osvaldo Miguel Colin
 */
public class CERNScopeRegistry extends ScopeRegistry {

    // SINGLETON
    private static CERNScopeRegistry INSTANCE;

    static {
        INSTANCE = null;
    }

    public static CERNScopeRegistry getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CERNScopeRegistry();
        }

        return INSTANCE;
    }

    // CLASS
    private CERNScopeRegistry() {
        super();
    }

}

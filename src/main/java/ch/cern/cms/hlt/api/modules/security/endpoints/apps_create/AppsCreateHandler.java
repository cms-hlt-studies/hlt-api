package ch.cern.cms.hlt.api.modules.security.endpoints.apps_create;

import ch.cern.cms.hlt.api.modules.security.SecurityModule;
import ch.cern.cms.hlt.api.modules.security.authentication.UserTokenAuthentication;
import ch.cern.cms.hlt.api.modules.security.resources.AppsFile;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientContext;
import ch.cern.cms.hlt.api.modules.security.security.CERNClientHandler;
import com.smmx.maria.commons.data.Tuple;

import static com.smmx.maria.commons.MariaCommons.dict;

public class AppsCreateHandler extends CERNClientHandler {

    public AppsCreateHandler() {
        this.registerAuthenticationMethod(new UserTokenAuthentication());
        this.permitScope(SecurityModule.ROLE_ADMIN);
    }

    @Override
    public void handle(CERNClientContext ctx) {
        // REQUEST
        AppsCreateRequest request = new AppsCreateRequest(ctx);

        // REGISTER
        Tuple credentials = AppsFile.getInstance().createApp(
            request.getName(),
            request.getDescription(),
            request.getPrivileges()
        );

        // REPLY
        ctx.result(dict(
            "PUBLIC_KEY", credentials.get(0, String.class),
            "SECRET_KEY", credentials.get(1, String.class)
        ));
    }

}

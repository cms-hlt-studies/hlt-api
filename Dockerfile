# ENVIRONMENT
FROM cern/cc7-base
RUN yum -y update
RUN yum -y install tar which java-1.8.0-openjdk-devel

# BUILD
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
RUN source ./docker/java_home.sh
RUN ./gradlew clean build

# INSTALL
RUN mkdir -p /usr/share/hlt-api
WORKDIR /usr/share/hlt-api
RUN tar xvf /usr/src/app/build/distributions/hlt-api-1.0.0.tar
RUN mv hlt-api-1.0.0/* . && rm -rf hlt-api-1.0.0
RUN mkdir -p /var/log/hlt-api
RUN mkdir -p /etc/hlt-api && cp -r /usr/src/app/local/* /etc/hlt-api
RUN rm -rf /usr/src/app

# EXPOSE
EXPOSE 8080

# ENTRYPOINT
ENTRYPOINT ["/usr/share/hlt-api/bin/hlt-api"]
CMD ["-d", "/etc/hlt-api", "-l", "/var/log/hlt-api/service.log"]
